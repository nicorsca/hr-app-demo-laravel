<?php


namespace Employee\Bank;


use App\Exceptions\ResourceExistsException;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\BankRepository;
use App\System\Common\Database\Models\Bank;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class BankCreateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\BankRepository
     */
    private BankRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new BankRepository(new Employee(), new \App\Modules\Employees\Database\Models\Bank());
    }

    public function testCreateEmployeeBank(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $data = Bank::factory()->raw();
        $this->repository->createBank($data, $employee->id);
        $data['bankable_id'] = $employee->id;
        $data['bankable_type'] = 'Employee';
        $this->assertDatabaseHas('banks', $data);
    }


    public function testBankShouldNotBeCreatedAgain(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $employee->bank()->create(Bank::factory()->raw());
        $data = Bank::factory()->raw();
        $this->expectException(ResourceExistsException::class);
        $this->repository->createBank($data, $employee->id);
    }

    /**
     * @throws \Exception
     */
    public function testCreateBankShouldNotCreateForUnknownEmployee(): void
    {
        $data = Bank::factory()->raw();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->createBank($data, random_int(1000, 2000));
    }
}
