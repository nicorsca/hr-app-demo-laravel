<?php


namespace Employee\AllocatedLeave;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\Setting;
use App\Modules\Employees\Repositories\AllocatedLeaveRepository;
use App\System\Employee\Database\Models\AllocatedLeave;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class AllocatedLeaveDetailUnitTest extends TestCase
{

    /**
     * @var \App\Modules\Employees\Repositories\AllocatedLeaveRepository
     */
    private AllocatedLeaveRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new AllocatedLeaveRepository(new Employee(), new \App\Modules\Employees\Database\Models\AllocatedLeave(), new Setting());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testLeaveDetailShouldFetch(): void
    {
        $leave = AllocatedLeave::factory()->create(['employee_id' => $this->employee->id]);
        $result = $this->repository->getLeaveById($this->employee->id, $leave->id);
        $this->assertInstanceOf(AllocatedLeave::class, $result);
        $this->assertEquals($result->id, $leave->id);
        $this->assertEquals($result->title, $leave->title);
        $this->assertEquals($result->days, $leave->days);
        $this->assertEquals($result->start_date, $leave->start_date);
        $this->assertEquals($result->end_date, $leave->end_date);
        $this->assertEquals($result->type, $leave->type);
        $this->assertEquals($result->status, $leave->status);
    }

    public function testLeaveDetailShouldNotFetchForUnknownEmployee(): void
    {
        $leave = AllocatedLeave::factory()->create(['employee_id' => $this->employee->id]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getLeaveById(random_int(10000, 20000), $leave->id);
    }

    public function testLeaveDetailShouldNotFetchForUnknownLeave(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getLeaveById($this->employee->id, random_int(10000, 20000));
    }


    public function testLeaveListShouldBeFetched(): void
    {
        $leave = AllocatedLeave::factory()->count(5)->create(['employee_id' => $this->employee->id]);
        $result = $this->repository->getLeaveList(['keyword' => ''], $this->employee->id);
        $this->assertCount(5, $result);
    }

    public function testDocumentListShouldNotBeFetchedForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getLeaveList([], random_int(1000, 2000));
    }

}
