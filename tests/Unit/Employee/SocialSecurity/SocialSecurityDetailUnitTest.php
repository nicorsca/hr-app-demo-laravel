<?php


namespace Employee\SocialSecurity;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\SocialSecurity;
use App\Modules\Employees\Repositories\SocialSecurityRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class SocialSecurityDetailUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\SocialSecurityRepository
     */
    private SocialSecurityRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new SocialSecurityRepository(new Employee(), new SocialSecurity());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testSocialSecurityDetailShouldFetch(): void
    {
        $socialSecurity = SocialSecurity::factory()->create(['employee_id' => $this->employee->id]);
        $result = $this->repository->getSocialSecurityById($this->employee->id, $socialSecurity->id);
        $this->assertInstanceOf(SocialSecurity::class, $result);
        $this->assertEquals($result->id, $socialSecurity->id);
        $this->assertEquals($result->title, $socialSecurity->title);
        $this->assertEquals($result->number, $socialSecurity->number);
        $this->assertEquals($result->start_date, $socialSecurity->start_date);
        $this->assertEquals($result->end_date, $socialSecurity->end_date);
        $this->assertEquals($result->status, $socialSecurity->status);
    }

    public function testSocialSecurityDetailShouldNotFetchForUnknownEmployee(): void
    {
        $socialSecurity = SocialSecurity::factory()->create(['employee_id' => $this->employee->id]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getSocialSecurityById(random_int(10000, 20000), $socialSecurity->id);
    }

    public function testSocialSecurityDetailShouldNotFetchForUnknownSocialSecurity(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getSocialSecurityById($this->employee->id, random_int(10000, 20000));
    }

    public function testSocialSecurityListShouldBeFetched(): void
    {
        SocialSecurity::factory()->count(5)->create(['employee_id' => $this->employee->id]);
        $result = $this->repository->getSocialSecurityList([], $this->employee->id);
        $this->assertCount(5, $result);
    }

    public function testSocialSecurityListShouldNotBeFetchedForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getSocialSecurityList([], random_int(1000, 2000));
    }

    public function testSocialSecurityListAreSortedAndOrderedAndSearchable(): void
    {
        $socialSecurities = SocialSecurity::factory()->count(3)->create(['employee_id' => $this->employee->id]);
        $socialSecurities = $socialSecurities->sortByDesc('title')->pluck('title');
        $result = $this->repository->getSocialSecurityList(['sort_by' => 'title', 'sort_order' => 'desc'], $this->employee->id);
        $result = $result->getCollection()->pluck('title');
        $this->assertEquals($socialSecurities, $result);

        $socialSecurity = SocialSecurity::factory()->create(['employee_id' => $this->employee->id]);
        $result = $this->repository->getSocialSecurityList(['keyword' => $socialSecurity->title], $this->employee->id);
        $this->assertEquals($socialSecurity->title, $result->first()->title);
    }

    public function testSocialSecurityListIsFilteredByStatus(): void
    {
        SocialSecurity::factory()->count(3)->create(['employee_id' => $this->employee->id]);
        SocialSecurity::factory()->count(2)->create(['employee_id' => $this->employee->id, 'status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->getSocialSecurityList(['status' => Status::STATUS_PUBLISHED, 'keyword' => ''], $this->employee->id);

        $this->assertCount(3, $result);
    }

    public function testSocialSecurityListEmptyWithWrongParameters(): void
    {
        SocialSecurity::factory()->count(3)->create(['employee_id' => $this->employee->id]);
        SocialSecurity::factory()->count(2)->create(['employee_id' => $this->employee->id, 'status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->getSocialSecurityList(['status' => 'xyz', 'keyword' => 'xyz'], $this->employee->id);

        $this->assertCount(0, $result);
    }
}
