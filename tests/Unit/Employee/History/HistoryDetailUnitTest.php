<?php


namespace Employee\History;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\History;
use App\Modules\Employees\Repositories\HistoryRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class HistoryDetailUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\HistoryRepository
     */
    private HistoryRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new HistoryRepository(new Employee(), new History());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }


    public function testHistoryListShouldBeFetched(): void
    {
        History::factory()->count(5)->create(['employee_id' => $this->employee->id]);
        $result = $this->repository->getHistoryList([], $this->employee->id);
        $this->assertCount(5, $result);
    }

    public function testHistoryListShouldNotBeFetchedForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getHistoryList([], random_int(1000, 2000));
    }

    public function testHistoryListAreSortedAndOrdered(): void
    {
        $histories = History::factory()->count(3)->create(['employee_id' => $this->employee->id]);

        $histories = $histories->sortByDesc('date')->pluck('date');
        $result = $this->repository->getHistoryList(['sort_by' => 'date', 'sort_order' => 'desc'], $this->employee->id);
        $result = $result->getCollection()->pluck('date');
        $this->assertEquals($histories, $result);
    }

    public function testHistoryListIsFilteredByStatus(): void
    {
        History::factory()->count(3)->create(['employee_id' => $this->employee->id, 'status' => Employee::STATUS_INACTIVE]);
        History::factory()->count(2)->create(['employee_id' => $this->employee->id, 'status' => Employee::STATUS_ACTIVE]);
        $result = $this->repository->getHistoryList(['status' => Employee::STATUS_ACTIVE, 'keyword' => ''], $this->employee->id);

        $this->assertCount(2, $result);
    }

    public function testHistoryListEmptyWithWrongParameters(): void
    {
        History::factory()->count(3)->create(['employee_id' => $this->employee->id, 'status' => Employee::STATUS_INACTIVE]);
        History::factory()->count(2)->create(['employee_id' => $this->employee->id, 'status' => Employee::STATUS_ACTIVE]);
        $result = $this->repository->getHistoryList(['status' => 'xyz'], $this->employee->id);

        $this->assertCount(0, $result);
    }
}
