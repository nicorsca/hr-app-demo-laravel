<?php


namespace Employee\Document;


use App\Events\Employee\Document\Created;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\DocumentRepository;
use App\System\Common\Database\Models\Document;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class DocumentCreateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\DocumentRepository
     */
    private DocumentRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new DocumentRepository(new Employee(), new \App\Modules\Employees\Database\Models\Document());
    }

    public function testCreateEmployeeDocument(): void
    {
        Event::fake();
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $data = Document::factory()->raw();
        $result = $this->repository->createDocument($data, $employee->id);
        $this->assertInstanceOf(Document::class, $result);
        $data['documentary_id'] = $employee->id;
        $data['documentary_type'] = 'Employee';
        $this->assertDatabaseHas('documents', $data);
        Event::assertDispatched(Created::class);
    }


    /**
     * @throws \Exception
     */
    public function testCreateDocumentShouldNotCreateForUnknownEmployee(): void
    {
        $data = Document::factory()->raw();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->createDocument($data, random_int(1000, 2000));
    }

}
