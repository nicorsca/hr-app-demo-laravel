<?php


namespace Employee\Address;


use App\Exceptions\ResourceExistsException;
use App\Modules\Employees\Database\Models\Address;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\AddressRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class AddressCreateUnitTest extends TestCase
{

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Repositories\AddressRepository
     */
    private AddressRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new AddressRepository(new Employee(), new Address());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testAddressShouldBeCreated(): void
    {
        $data = Address::factory()->raw();
        $result = $this->repository->createAddress($data, $this->employee->id);
        $this->assertInstanceOf(Address::class, $result);
        $data['addressable_id'] = $this->employee->id;
        $data['addressable_type'] = 'Employee';
        $this->assertDatabaseHas('addresses', $data);
    }

    public function testMultipleAddressShouldNotCreateOfSameType(): void
    {
        $data = Address::factory()->raw();
        $this->repository->createAddress($data, $this->employee->id);
        $this->expectException(ResourceExistsException::class);
        $this->repository->createAddress($data, $this->employee->id);
    }

    /**
     * @throws \Exception
     */
    public function testCreateAddressShouldNotCreateForUnknownEmployee(): void
    {
        $data = Address::factory()->raw();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->createAddress($data, random_int(1000, 2000));
    }

}
