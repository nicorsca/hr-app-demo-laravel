<?php


namespace Leave;


use App\Modules\Leaves\Database\Models\AllocatedLeave;
use App\Modules\Leaves\Database\Models\Leave;
use App\Modules\Leaves\Database\Models\Setting;
use App\Modules\Leaves\Mails\LeaveRequestMail;
use App\Modules\Leaves\Repositories\LeaveRepository;
use App\System\Employee\Database\Models\Employee;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;
use NicoSystem\Exceptions\NicoBadRequestException;
use Tests\TestCase;

class LeaveRequestUnitTest extends TestCase
{
    private Employee $employee;

    private Employee $manager;

    private Setting $leaveType;

    private string $employeeAccessToken;

    private LeaveRepository $repository;

    public function testEmployeeCanRequestForLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 2
        ]);
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::tomorrow()->addDay()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[1],
            'days' => 5,
        ]);
        //leave request for 2 days
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $this->assertInstanceOf(Leave::class, $response->getOriginalContent());
        $data['status'] = LeaveStatus::PENDING;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCannotRequestForLeaveIfEmployeeIdAndRequestedIdIsSame(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::yesterday()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
        ]);
        $data = Leave::factory()->raw([
            'requested_to' => $this->employee->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(400);
        $response->assertJson([
            'code' => 'err_cannot_leave_request_self',
            'code_text' => trans('responses.employee.cannot_request_self'),
        ]);
    }

    public function testEmployeeCannotRequestForPaidLeaveIfAllocatedLeaveIsFinished(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(2)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 2
        ]);
        //leave for 2 days
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->subMonth()->addDay()->toDateTimeString(),
            'end_at' => Carbon::now()->subMonth()->addDays(2)->subSecond()->toDateTimeString(),
            'days' => 2
        ]);
        //request 2 more days leave
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);

        $response->assertStatus(400);
        $response->assertJson([
            'code' => 'paid_leave_exceed_exception',
            'code_text' => trans('responses.leave.paid_leave_exceed'),
        ]);
    }

    public function testEmployeeCannotRequestForPaidLeaveIfAllocatedLeaveIsAbsent(): void
    {
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(404);
        $response->assertJson([
            'code' => 'allocated_leave_not_found',
            'code_text' => trans('responses.leave.empty_allocated_leave'),
        ]);
    }

    public function testEmployeeCannotRequestForPaidLeaveIfLeavesAreRequestedMoreThanAllocatedLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 1
        ]);
        //leave taken for 1 day
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->subMonth()->addDay()->toDateTimeString(),
            'end_at' => Carbon::now()->subMonth()->addDays(1)->subSecond()->toDateTimeString(),
            'days' => 1
        ]);
        //request 2 more days leave
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(400);
        $response->assertJson([
            'code' => 'paid_leave_exceed_exception',
            'code_text' => trans('responses.leave.paid_leave_exceed'),
        ]);
    }

    public function testEmployeeCannotRequestForPaidLeaveIfAlreadyLeaveIsRequestedInThatDateRange(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 5
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(409);
        $response->assertJson([
            'code' => 'leave_exists_exception',
            'code_text' => trans('responses.leave.requested_already'),
        ]);
    }

    public function testEmployeeCannotRequestForPaidLeaveIfAlreadyLeaveIsRequestedInThatPartialDateRange(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(6)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(409);
        $response->assertJson([
            'code' => 'leave_exists_exception',
            'code_text' => trans('responses.leave.requested_already'),
        ]);

        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(409);
        $response->assertJson([
            'code' => 'leave_exists_exception',
            'code_text' => trans('responses.leave.requested_already'),
        ]);
    }

    public function testLeaveRequestShouldSplitIfLeaveIsRequestedAtStartAndEndOfFiscalYear(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonths(5)->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave request for 2+3 days
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addMonths(6)->subDays(2)->toDateString() . ' 00:00:00',
            'end_at' => Carbon::now()->addMonths(6)->addDays(2)->subSecond()->toDateString() . ' 23:59:59',
        ]);
        //allocate leave for next fiscal year
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->addMonths(7)->toDateString(),
            'end_date' => Carbon::now()->addMonths(12)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(200);
        $response->assertJsonCount(2);

        $data['status'] = LeaveStatus::PENDING;
        $fiscalDateStartSetting = Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->first();
        $fiscalDateStart = Carbon::now()->year . '-' . $fiscalDateStartSetting->value;
        $fiscalDateEnd = Carbon::parse($fiscalDateStart)->addYear()->subDay()->toDateString();

        $endAt = $data['end_at'];
        $data['end_at'] = $fiscalDateEnd . ' 23:59:59';
        $this->assertDatabaseHas('leaves', $data);

        $fiscalDateStart = Carbon::parse($fiscalDateEnd)->addDay()->toDateString();
        $data['start_at'] = $fiscalDateStart . ' 00:00:00';
        $data['end_at'] = $endAt;
        $data['days'] = 3;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCanRequestForUnpaidLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 2
        ]);
        //leave for 2 days
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->subMonth()->addDay()->toDateTimeString(),
            'end_at' => Carbon::now()->subMonth()->addDays(2)->subSecond()->toDateTimeString(),
            'days' => 2
        ]);
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
        ]);

        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(200);
        $this->assertInstanceOf(Leave::class, $response->getOriginalContent());
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCanRequestForUnpaidLeaveForEndAndStartOfFiscalYear(): void
    {
        //leave request for 2+3 days
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'start_at' => Carbon::now()->addMonths(6)->subDays(2)->toDateString() . ' 00:00:00',
            'end_at' => Carbon::now()->addMonths(6)->addDays(2)->subSecond()->toDateString() . ' 23:59:59',
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(200);
        $response->assertJsonCount(2);

        $data['status'] = LeaveStatus::PENDING;
        $fiscalDateStartSetting = Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->first();
        $fiscalDateStart = Carbon::now()->year . '-' . $fiscalDateStartSetting->value;
        $fiscalDateEnd = Carbon::parse($fiscalDateStart)->addYear()->subDay()->toDateString();

        $endAt = $data['end_at'];
        $data['end_at'] = $fiscalDateEnd . ' 23:59:59';
        $this->assertDatabaseHas('leaves', $data);

        $fiscalDateStart = Carbon::parse($fiscalDateEnd)->addDay()->toDateString();
        $data['start_at'] = $fiscalDateStart . ' 00:00:00';
        $data['end_at'] = $endAt;
        $data['days'] = 3;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCannotRequestForUnpaidLeaveIfLeaveIsAlreadyRequestInThatDateRange(): void
    {
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
        ]);
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE
        ]);
        $response = $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(409);
        $response->assertJson([
            'code' => 'leave_exists_exception',
            'code_text' => trans('responses.leave.requested_already'),
        ]);
    }

    public function testEmployeeCanRequestCancellationLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'status' => LeaveStatus::APPROVED
        ]);
        $data = Leave::factory()->raw([
            'type' => LeaveType::CANCELLATION_LEAVE,
            'employee_id' => $this->employee->id,
            'parent_id' => $leave->id,
            'parent_type' => $leave->type,
        ]);
        $result = $this->repository->requestCancellationLeave(Arr::only($data, ['title', 'description', 'start_at', 'end_at', 'employee_id', 'type']), $leave->id);
        $this->assertInstanceOf(Leave::class, $result);
        $data['requested_to'] = $leave->requested_to;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCanRequestCancellationLeaveWithinRequestedLeaveDateRange(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'status' => LeaveStatus::APPROVED
        ]);
        $data = Leave::factory()->raw([
            'start_at' => Carbon::today()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(9)->subSecond()->format('Y-m-d H:i:s'),
            'type' => LeaveType::CANCELLATION_LEAVE,
            'employee_id' => $this->employee->id,
        ]);
        $result = $this->repository->requestCancellationLeave(Arr::only($data, ['title', 'description', 'start_at', 'end_at','employee_id', 'type']), $leave->id);
        $this->assertInstanceOf(Leave::class, $result);
        $data['days'] = 1;
        $data['requested_to'] = $leave->requested_to;
        $data['employee_id'] = $this->employee->id;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCannotRequestCancellationLeaveForExistingCancellationLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'status' => LeaveStatus::APPROVED
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
            'status' => LeaveStatus::PENDING
        ]);
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
        ]);
        $response = $this->json('POST', 'api/leaves/' . $leave->id . '/cancellation', $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(409);
        $response->assertJson([
            'code' => 'leave_exists_exception',
            'code_text' => trans('responses.leave.requested_already'),
        ]);
    }

    public function testEmployeeCannotRequestCancellationLeaveIfLeavesDoNotExist(): void
    {
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
        ]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->requestCancellationLeave(Arr::only($data, ['title', 'description', 'start_at', 'end_at', 'employee_id', 'type']), random_int(1000, 2000));
    }

    public function testEmployeeCannotRequestIfCancellationLeaveIsNotInRangeOfLeaveTakenDay(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'status' => LeaveStatus::APPROVED
        ]);
        $data = Leave::factory()->raw([
            'start_at' => Carbon::today()->addDays(1)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(3)->subSecond()->format('Y-m-d H:i:s'),
            'type' => LeaveType::CANCELLATION_LEAVE,
            'employee_id' => $this->employee->id,
        ]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->requestCancellationLeave(Arr::only($data, ['title', 'description', 'start_at', 'end_at','employee_id', 'type']), $leave->id);
    }

    public function testCancellationLeaveShouldNotBeRequestedIfLeaveIsNotApproved(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $data = Leave::factory()->raw([
            'type' => LeaveType::CANCELLATION_LEAVE,
            'employee_id' => $this->employee->id,
        ]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->requestCancellationLeave(Arr::only($data, ['title', 'description', 'start_at', 'end_at', 'employee_id', 'type']), $leave->id);
    }

    public function testMultipleCancellationLeaveCanBeRequestedInOneLeaveRequest(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 5 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'status' => LeaveStatus::APPROVED,
            'start_at' => Carbon::now()->addDays(10)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(15)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        //cancellation leave for 2 days
        $data = Leave::factory()->raw([
            'type' => LeaveType::CANCELLATION_LEAVE,
            'employee_id' => $this->employee->id,
            'start_at' => Carbon::now()->addDays(10)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(12)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $result = $this->repository->requestCancellationLeave(Arr::only($data, ['title', 'description', 'start_at', 'end_at', 'employee_id', 'type']), $leave->id);
        $this->assertInstanceOf(Leave::class, $result);
        $data['requested_to'] = $leave->requested_to;
        $this->assertDatabaseHas('leaves', $data);

        //cancellation leave for 3 days
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
            'start_at' => Carbon::now()->addDays(12)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(15)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $result = $this->repository->requestCancellationLeave(Arr::only($data, ['title', 'description', 'start_at', 'end_at', 'employee_id', 'type']), $leave->id);
        $this->assertInstanceOf(Leave::class, $result);
        $data['requested_to'] = $leave->requested_to;
        $data['days'] = 3;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testMailShouldBeSentDuringLeaveRequest(): void
    {
        Mail::fake();
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 2
        ]);
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::tomorrow()->addDay()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[1],
            'days' => 5,
        ]);
        //leave request for 2 days
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $this->json('POST', 'api/leaves/', $data, ['authorization' => $this->employeeAccessToken]);
        Mail::assertQueued(LeaveRequestMail::class);
    }

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->employeeAccessToken = 'Bearer ' . $this->employee->user->createToken('test12345')->accessToken;;
        $this->manager = Employee::factory()->forUser()->forDepartment()->create();
        $this->leaveType = Setting::where('key', SettingKey::LEAVES_TYPE)->first();
        Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->update(['value' => Carbon::now()->addMonths(6)->format('m-d')]);
        $this->repository = new LeaveRepository(new Leave(), new AllocatedLeave(), new Setting(), new \App\Modules\Leaves\Database\Models\Employee());
    }

}
