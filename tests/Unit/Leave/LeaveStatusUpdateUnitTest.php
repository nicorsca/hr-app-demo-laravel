<?php


namespace Leave;


use App\Modules\Leaves\Database\Models\AllocatedLeave;
use App\Modules\Leaves\Database\Models\Leave;
use App\Modules\Leaves\Database\Models\Setting;
use App\Modules\Leaves\Exceptions\InvalidLeaveStatusException;
use App\Modules\Leaves\Mails\ApproveLeaveRequestMail;
use App\Modules\Leaves\Mails\CancelLeaveRequestMail;
use App\Modules\Leaves\Repositories\LeaveRepository;
use App\System\AppConstants;
use App\System\Attendance\Database\Models\Attendance;
use App\System\Attendance\Foundation\AttendanceType;
use App\System\Employee\Database\Models\Employee;
use App\System\Foundation\Scope\GlobalEmployeeScope;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Foundation\SettingKey;
use App\System\User\Database\Models\User;
use App\System\User\Foundation\RoleName;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;
use NicoSystem\Exceptions\NicoBadRequestException;
use Tests\TestCase;

class LeaveStatusUpdateUnitTest extends TestCase
{
    private LeaveRepository $repository;

    private Employee $employee;

    private Employee $manager;

    private Setting $leaveType;

    private string $employeeAccessToken;

    private string $managerAccessToken;


    public function testRequestedEmployeeCanDeclineEmployeeRequestedLeave(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $data = [
            'reason' => $this->faker->paragraph
        ];
        $result = $this->repository->declineLeaveRequest($data, $leave->id, $this->manager->user);
        $this->assertInstanceOf(Leave::class, $result);
        $data += $leave->toArray();
        $data['status'] = LeaveStatus::DECLINED;
        unset($data['created_at'], $data['updated_at']);
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testMailShouldBeSentWhenRequestedEmployeeDeclineEmployeeRequestedLeave(): void
    {
        Mail::fake();
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $data = [
            'reason' => $this->faker->paragraph
        ];
        $this->repository->declineLeaveRequest($data, $leave->id, $this->manager->user);
        Mail::assertQueued(CancelLeaveRequestMail::class);
    }

    public function testEmployeeCanSelfCancelRequestedLeave(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $employee->user->assignRole(RoleName::EMPLOYEE);
        $manager = Employee::factory()->forUser()->forDepartment()->create();
        $manager->user->assignRole(RoleName::MANAGER);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $data = [
            'reason' => $this->faker->paragraph
        ];
        $accessToken = 'Bearer ' . $this->employee->user->createToken('test')->accessToken;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/cancel', $data, ['authorization' => $accessToken]);
        $response->assertStatus(200);
        $data += $leave->toArray();
        $data['status'] = LeaveStatus::CANCELLED;
        unset($data['updated_at']);
        $response->assertJson($data);
        $this->assertDatabaseHas('leaves', Arr::only($data, ['id', 'title', 'status', 'reason', 'start_at', 'end_at']));
    }

    public function testMailShouldBeSentWhenEmployeeCancelOwnLeaveRequest(): void
    {
        Mail::fake();
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $data = [
            'reason' => $this->faker->paragraph
        ];
        $this->repository->cancelLeaveRequest($data, $leave->id, $this->employee->user);
        Mail::assertQueued(CancelLeaveRequestMail::class);
    }

    public function testUnauthorizedEmployeeCannotCancelEmployeeRequestedLeave(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $data = [
            'reason' => $this->faker->paragraph
        ];
        $user = User::factory()->create();
        $this->expectException(NicoBadRequestException::class);
        $this->repository->cancelLeaveRequest($data, $leave->id, $user);
    }

    public function testAuthorizedEmployeeCannotCancelLeaveRequestUntilItsStatusIsPending(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'status' => LeaveStatus::CANCELLED,
        ]);
        $data = [
            'reason' => $this->faker->paragraph
        ];
        $this->expectException(InvalidLeaveStatusException::class);
        $this->repository->cancelLeaveRequest($data, $leave->id, $this->employee->user);
    }

    public function testEmployeeCannotCancelLeaveIfLeaveIsExpired(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::yesterday()->toDateTimeString(),
        ]);
        $data = [
            'reason' => $this->faker->paragraph
        ];
        $this->expectException(NicoBadRequestException::class);
        $this->repository->cancelLeaveRequest($data, $leave->id, $this->employee->user);
    }

    public function testRequestedManagerCanApproveEmployeeRequestedLeave(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $result = $this->repository->approveLeaveRequest($leave->id, $this->manager->user);
        $this->assertInstanceOf(Leave::class, $result);
        $data = $leave->toArray();
        $data['status'] = LeaveStatus::APPROVED;
        unset($data['created_at']);
        unset($data['updated_at']);
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testMailShouldBeSentWhenLeaveRequestIsApproved(): void
    {
        Mail::fake();
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $accessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/approve', [], ['authorization' => $accessToken]);
        $response->assertStatus(200);
        Mail::assertQueued(ApproveLeaveRequestMail::class);
    }

    public function testEmployeeCannotSelfApproveRequestedLeave(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->approveLeaveRequest($leave->id, $this->employee->user);
    }

    public function testAuthorizedEmployeeCannotApproveLeaveRequestUntilItsStatusIsPending(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'status' => LeaveStatus::APPROVED,
        ]);
        $accessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/approve', [], ['authorization' => $accessToken]);
        $response->assertStatus(422);
        $response->assertJson([
            'code' => AppConstants::ERR_INVALID_LEAVE_STATUS_EXCEPTION,
            'code_text' => trans('responses.leave.status_not_pending')
        ]);
    }

    public function testEmployeeCannotApproveLeaveIfLeaveIsExpired(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::yesterday()->toDateTimeString(),
        ]);
        $accessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/approve', [], ['authorization' => $accessToken]);
        $response->assertStatus(400);
        $response->assertJson([
            'code' => AppConstants::ERR_LEAVE_EXPIRED_EXCEPTION,
            'code_text' => trans('responses.leave.leave_expired')
        ]);
    }

    public function testWhenLeaveRequestIsApprovedAttendanceShouldBeUpdatedForMultipleDays(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(10)->subSecond()->subHours(12)->format('Y-m-d H:i:s'),
        ]);
        $accessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/approve', [], ['authorization' => $accessToken]);
        $response->assertStatus(200);
        $data = $leave->toArray();
        $data['status'] = LeaveStatus::APPROVED;
        unset($data['created_at']);
        unset($data['updated_at']);
        $this->assertDatabaseHas('leaves', $data);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(8)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);

        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(9)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(9)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(10)->subSecond()->subHours(12)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
    }

    public function testWhenLeaveRequestIsApprovedAttendanceShouldBeUpdatedForHalfDay(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(8)->subSecond()->subHours(12)->format('Y-m-d H:i:s'),
        ]);
        $accessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/approve', [], ['authorization' => $accessToken]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(8)->subSecond()->subHours(12)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
    }

    public function testWhenLeaveRequestIsApprovedAttendanceShouldBeUpdatedForFullDay(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $accessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/approve', [], ['authorization' => $accessToken]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
    }

    public function testWhenEmployeeCancellationLeaveRequestIsApprovedAttendanceShouldBeDeleted(): void
    {
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(10)->subSecond()->subHours(12)->format('Y-m-d H:i:s'),
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
            'start_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(10)->subSecond()->subHours(12)->format('Y-m-d H:i:s'),
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(8)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(9)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(9)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(10)->subSecond()->subHours(12)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $attendances = Attendance::all();
        $accessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/approve', [], ['authorization' => $accessToken]);
        $response->assertStatus(200);
        foreach ($attendances as $attendance) {
            $this->assertNotNull($attendance->fresh()->deleted_at);
        }
    }

    public function testWhenEmployeeCancellationLeaveRequestIsApprovedAttendanceShouldBePartiallyDeleted(): void
    {
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(10)->subSecond()->subHours(12)->format('Y-m-d H:i:s'),
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
            'start_at' => Carbon::today()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(9)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(8)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(9)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(9)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(10)->subSecond()->subHours(12)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $accessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/approve', [], ['authorization' => $accessToken]);
        $response->assertStatus(200);
        $this->assertCount(4, Attendance::all());
    }

    public function testWhenEmployeeFirstHalfDayCancellationLeaveRequestIsApprovedAttendanceShouldBeUpdated(): void
    {
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::today()->addDays(7)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
            'start_at' => Carbon::today()->addDays(7)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(8)->subSecond()->subHours(12)->format('Y-m-d H:i:s'),
        ]);
        $leaveInAttendance = Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(7)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $leaveOutAttendance = Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $accessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/approve', [], ['authorization' => $accessToken]);
        $response->assertStatus(200);
        $attendances = Attendance::all();
        $this->assertCount(2, $attendances);
        $this->assertEquals(Carbon::today()->addDays(7)->addHours(12)->toDateTimeString(), $leaveInAttendance->fresh()->attend_at);
        $this->assertEquals(Carbon::today()->addDays(8)->subSecond()->toDateTimeString(), $leaveOutAttendance->fresh()->attend_at);
    }

    public function testWhenEmployeeSecondHalfDayCancellationLeaveRequestIsApprovedAttendanceShouldBeUpdated(): void
    {
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::today()->addDays(7)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
            'start_at' => Carbon::today()->addDays(7)->addHours(12)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $leaveInAttendance = Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(7)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $leaveOutAttendance = Attendance::factory()->create([
            'attend_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $accessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $response = $this->json('PUT', 'api/leaves/' . $leave->id . '/approve', [], ['authorization' => $accessToken]);
        $response->assertStatus(200);
        $attendances = Attendance::all();
        $this->assertCount(2, $attendances);
        $this->assertEquals(Carbon::today()->addDays(7)->toDateTimeString(), $leaveInAttendance->fresh()->attend_at);
        $this->assertEquals(Carbon::today()->addDays(8)->subSecond()->subHours(12)->toDateTimeString(), $leaveOutAttendance->fresh()->attend_at);
    }


    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->employee->user->assignRole(RoleName::EMPLOYEE);
        $this->employeeAccessToken = 'Bearer ' . $this->employee->user->createToken('test')->accessToken;
        $this->manager = Employee::factory(['personal_email'=>"testuser@ensue.com.np"])->forUser(['first_name'=>"testUser"])->forDepartment()->create();
        $this->manager->user->assignRole(RoleName::MANAGER);
        $this->managerAccessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;
        $this->leaveType = Setting::where('key', SettingKey::LEAVES_TYPE)->first();
        Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->update(['value' => Carbon::now()->addMonths(6)->format('m-d')]);

        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        $this->repository = new LeaveRepository(new Leave(), new AllocatedLeave(), new Setting(), new \App\Modules\Leaves\Database\Models\Employee());
    }
}
