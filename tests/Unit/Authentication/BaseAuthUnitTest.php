<?php


namespace Authentication;


use Illuminate\Support\Facades\DB;
use Tests\TestCase;

abstract class BaseAuthUnitTest extends TestCase
{

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        DB::table('oauth_clients')->insert([
            'user_id' => env('AUTH_PASSWORD_CLIENT_ID', 3),
            'name' => 'test',
            'secret' => env('AUTH_PASSWORD_CLIENT_SECRET', 'Ex7bBCXxOu1GTxmeaFv1hgnhbxefR3FcwAPWGJZL'),
            'redirect' => 'http://localhost',
            'password_client' => '1',
            'personal_access_client' => '0',
            'revoked' => '0',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
