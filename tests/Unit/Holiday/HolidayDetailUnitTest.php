<?php

namespace Holiday;

use App\Modules\Holidays\Database\Models\Holiday;
use App\Modules\Holidays\Repositories\HolidayRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class HolidayDetailUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Holidays\Repositories\HolidayRepository
     */
    private HolidayRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new HolidayRepository(new Holiday());
    }

    public function testHolidayShouldDetail(): void
    {
        $holiday = Holiday::factory()->create();

        $result = $this->repository->getById($holiday->id);
        $this->assertInstanceOf(Holiday::class, $result);
        $this->assertEquals($holiday->id, $result->id);
        $this->assertEquals($holiday->title, $result->title);
        $this->assertEquals($holiday->date, $result->date);
        $this->assertEquals($holiday->remarks, $result->remarks);
    }

    public function testHolidayShouldNotDetailForUnknownHoliday(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getById(random_int(1000, 2000));
    }


    public function testHolidayShouldList(): void
    {
        for ($i = 0; $i<2; $i++) {
            Holiday::factory()->create();
        }

        $result = $this->repository->getList(['keyword' => '', 'status' => '', 'title' => '', 'start_date' => '', 'end_date' => '']);
        $this->assertCount(2, $result);
    }

    public function testHolidayShouldListForDateRange(): void
    {
        for ($i = 0; $i<3; $i++) {
            Holiday::factory()->create(['date' => $this->faker->unique()->dateTimeThisMonth]);
        }
        Holiday::factory()->create(['date' => Carbon::now()->subYear()->toDateString()]);

        $result = $this->repository->getList(['start_date' => Carbon::now()->subMonth()->toDateString(), 'end_date' => Carbon::now()->addMonth()->toDateString()]);
        $this->assertCount(3, $result);
    }

    public function testHolidayShouldSearchThroughTitleOrKeyword(): void
    {
        for ($i = 0; $i<3; $i++) {
            Holiday::factory()->create(['date' => $this->faker->unique()->dateTimeThisMonth]);
        }
        $holiday = Holiday::factory()->create(['date' => Carbon::now()->subYear()->toDateString()]);

        $result = $this->repository->getList(['keyword' => $holiday->title]);
        $this->assertCount(1, $result);
    }

    public function testHolidayListShouldBeEmptyForWrongInputs(): void
    {
        for ($i = 0; $i<3; $i++) {
            Holiday::factory()->create(['date' => $this->faker->unique()->dateTimeThisMonth]);
        }
        Holiday::factory()->create(['date' => Carbon::now()->subYear()->toDateString()]);

        $result = $this->repository->getList(['keyword' => 'test-xyz']);
        $this->assertCount(0, $result);
    }
}
