<?php


namespace Project\Document;


use App\Events\Project\Document\Created;
use App\Modules\Projects\Database\Models\Document;
use App\Modules\Projects\Database\Models\Project;
use App\Modules\Projects\Repositories\DocumentRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class DocumentCreateUnitTest extends TestCase
{

    /**
     * @var DocumentRepository
     */
    private DocumentRepository $repository;

    /**
     * @var \App\System\Project\Database\Models\Project
     */
    private \App\System\Project\Database\Models\Project $project;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new DocumentRepository(new Project(), new Document());
        $this->project = Project::factory()->forClient()->create();
    }

    public function testCreateDocument(): void
    {
        Event::fake();
        $data = Document::factory()->raw();
        $result = $this->repository->createDocument($data, $this->project->id);
        $this->assertInstanceOf(Document::class, $result);
        $data['documentary_id'] = $this->project->id;
        $data['documentary_type'] = 'Project';
        $this->assertDatabaseHas('documents', $data);
        Event::assertDispatched(Created::class);
    }


    /**
     * @throws \Exception
     */
    public function testCreateDocumentShouldNotCreateForUnknownProject(): void
    {
        $data = Document::factory()->raw();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->createDocument($data, random_int(1000, 2000));
    }

}
