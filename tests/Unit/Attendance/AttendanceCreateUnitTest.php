<?php

namespace Attendance;

use App\Modules\Attendances\Database\Models\Attendance;
use App\Modules\Attendances\Database\Models\Setting;
use App\Modules\Attendances\Repositories\AttendanceRepository;
use App\System\Attendance\Foundation\AttendanceType;
use App\System\Employee\Database\Models\Employee;
use Carbon\Carbon;
use NicoSystem\Exceptions\NicoBadRequestException;
use Tests\TestCase;

class AttendanceCreateUnitTest extends TestCase
{

    private AttendanceRepository $repository;

    private Employee $employee;

    public function testEmployeeCanCheckIn(): void
    {
        $data = Attendance::factory()->raw(['employee_id' => $this->employee->id]);
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertEquals($data['attend_at'], $result->attend_at);
        $this->assertEquals($data['type'], $result->type);
        $this->assertEquals(null, $result->reason);
        $this->assertEquals($data['browser'], $result->browser);
        $this->assertEquals($data['device'], $result->device);
        $this->assertEquals($data['location'], $result->location);
        $this->assertEquals($data['ip'], $result->ip);
        $this->assertEquals($data['fingerprint'], $result->fingerprint);
        $this->assertEquals($data['punch_count'], $result->punch_count);
        $this->assertDatabaseHas('attendances', $data);
    }

    public function testEmployeeCannotCheckInIfAlreadyCheckedIn(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->unix() * 1000,
            'employee_id' => $this->employee->id
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::now()->unix() * 1000,
            'employee_id' => $this->employee->id]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->create($data);
    }

    public function testEmployeeCanCheckInIfOnlyCheckout(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 18:00:00',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT
        ]);
        $data = Attendance::factory()->raw(['employee_id' => $this->employee->id]);
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertDatabaseHas('attendances', $data);
    }

    public function testEmployeeCannotCheckInAfterLeaveIn(): void
    {
        Attendance::factory()->create([
            'type' => AttendanceType::LEAVE_IN,
            'employee_id' => $this->employee->id,
            'attend_at' => Carbon::today(),
        ]);
        Attendance::factory()->create([
            'type' => AttendanceType::LEAVE_OUT,
            'employee_id' => $this->employee->id,
            'attend_at' => Carbon::today()->toDateString() . ' 23:59.59',
        ]);
        $data = Attendance::factory()->raw(['employee_id' => $this->employee->id]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->create($data);
    }

    public function testEmployeeCanCheckInAfterLeaveOut(): void
    {
        Attendance::factory()->create([
            'type' => AttendanceType::LEAVE_IN,
            'employee_id' => $this->employee->id,
            'attend_at' => Carbon::yesterday()->toDateString() . ' 00:00:00',
        ]);
        Attendance::factory()->create([
            'type' => AttendanceType::LEAVE_OUT,
            'employee_id' => $this->employee->id,
            'attend_at' => Carbon::today()->subSecond()->toDateTimeString(),
        ]);
        $data = Attendance::factory()->raw(['employee_id' => $this->employee->id]);
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertDatabaseHas('attendances', $data);
    }

    public function testEmployeeCanCheckInAfterHalfLeaveFinish(): void
    {
        Attendance::factory()->create([
            'type' => AttendanceType::LEAVE_IN,
            'employee_id' => $this->employee->id,
            'attend_at' => Carbon::today()->toDateString() . ' 00:00:00',
        ]);
        Attendance::factory()->create([
            'type' => AttendanceType::LEAVE_OUT,
            'employee_id' => $this->employee->id,
            'attend_at' => Carbon::today()->toDateString() . ' 12:00:00',
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateString() . ' 12:00:01',
            'employee_id' => $this->employee->id
        ]);
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertDatabaseHas('attendances', $data);
    }

    public function testEmployeeCannotCheckInDuringBreakOut(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'type' => AttendanceType::BREAK_OUT,
            'attend_at' => Carbon::today()->toDateString() . ' 01:00:00',
            'employee_id' => $this->employee->id
        ]);
        $this->expectException(NicoBadRequestException::class);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateString() . ' 02:00:00',
            'employee_id' => $this->employee->id
        ]);
        $this->repository->create($data);
    }

    public function testEmployeeCannotCheckInDuringBreakIn(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 01:00:00',
            'type' => AttendanceType::BREAK_OUT,
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 02:00:00',
            'type' => AttendanceType::BREAK_IN,
            'employee_id' => $this->employee->id
        ]);
        $this->expectException(NicoBadRequestException::class);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateString() . ' 03:00:00',
            'employee_id' => $this->employee->id
        ]);
        $this->repository->create($data);
    }

    public function testEmployeeCanCheckoutAfterCheckIn(): void
    {
        Attendance::factory()->create(['employee_id' => $this->employee->id]);
        $data = Attendance::factory()->raw([
            'type' => AttendanceType::CHECK_OUT,
            'employee_id' => $this->employee->id,
            'attend_at' => Carbon::today()->toDateString() . '01:00:00',
        ]);
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertEquals($data['attend_at'], $result->attend_at);
        $this->assertEquals($data['type'], $result->type);
        $this->assertEquals(null, $result->reason);
        $this->assertEquals($data['browser'], $result->browser);
        $this->assertEquals($data['device'], $result->device);
        $this->assertEquals($data['location'], $result->location);
        $this->assertEquals($data['ip'], $result->ip);
        $this->assertDatabaseHas('attendances', $data);
    }

    public function testEmployeeCannotCheckoutWithoutCheckIn(): void
    {
        $data = Attendance::factory()->raw(['employee_id' => $this->employee->id, 'type' => AttendanceType::CHECK_OUT]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->create($data);
    }

    public function testEmployeeCannotCheckoutIfAlreadyCheckout(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 18:00:00',
            'type' => AttendanceType::CHECK_OUT,
            'employee_id' => $this->employee->id
        ]);
        $data = Attendance::factory()->raw(['type' => AttendanceType::CHECK_OUT, 'employee_id' => $this->employee->id]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->create($data);
    }

    public function testEmployeeCannotCheckoutAfterBreakOut(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 12:00:00',
            'type' => AttendanceType::BREAK_OUT,
            'employee_id' => $this->employee->id
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateString() . ' 09:00:00',
            'type' => AttendanceType::CHECK_OUT,
            'employee_id' => $this->employee->id
        ]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->create($data);
    }

    public function testEmployeeCanCheckoutAfterBreakIn(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 12:00:00',
            'type' => AttendanceType::BREAK_OUT,
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 13:00:00',
            'type' => AttendanceType::BREAK_IN,
            'employee_id' => $this->employee->id
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateString() . ' 14:00:00',
            'type' => AttendanceType::CHECK_OUT,
            'employee_id' => $this->employee->id,
        ]);
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertDatabaseHas('attendances', $data);
    }

    public function testEmployeeCanBreakOutAfterCheckIn(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateString() . ' 01:00:00',
            'type' => AttendanceType::BREAK_OUT,
            'employee_id' => $this->employee->id
        ]);
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertDatabaseHas('attendances', $data);
    }

    public function testEmployeeCannotBreakOutAgain(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 00:00:01',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::BREAK_OUT,
        ]);
        $this->expectException(NicoBadRequestException::class);
        $data = Attendance::factory()->raw([
            'type' => AttendanceType::BREAK_OUT,
            'employee_id' => $this->employee->id,
            'attend_at' => Carbon::today()->toDateString() . ' 01:00:00',
        ]);
        $this->repository->create($data);
    }

    public function testEmployeeCannotBreakOutAfterCheckout(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 18:00:01',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $this->expectException(NicoBadRequestException::class);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateString() . ' 00:00:01',
            'type' => AttendanceType::BREAK_OUT,
            'employee_id' => $this->employee->id
        ]);
        $this->repository->create($data);
    }

    public function testEmployeeCanBreakOutAfterBreakIn(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 00:00:01',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::BREAK_OUT,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 00:00:02',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::BREAK_IN,
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateString() . ' 01:00:00',
            'type' => AttendanceType::BREAK_OUT,
            'employee_id' => $this->employee->id,
            'punch_count' => 2,
        ]);
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertDatabaseHas('attendances', $data);
    }

    public function testEmployeeCanBreakInAfterBreakOut(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 00:00:01',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::BREAK_OUT,
        ]);
        $data = Attendance::factory()->raw([
            'type' => AttendanceType::BREAK_IN,
            'employee_id' => $this->employee->id,
            'attend_at' => Carbon::today()->toDateString() . ' 01:00:00',
        ]);
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertDatabaseHas('attendances', $data);
    }

    public function testEmployeeCannotBreakInAgain(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 00:00:01',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::BREAK_OUT,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 00:00:02',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::BREAK_IN,
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateString() . ' 00:00:05',
            'type' => AttendanceType::BREAK_IN,
            'employee_id' => $this->employee->id
        ]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->create($data);
    }

    public function testEmployeeCannotBreakInAfterCheckIn(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateString() . ' 00:00:01',
            'type' => AttendanceType::BREAK_IN,
            'employee_id' => $this->employee->id
        ]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->create($data);
    }

    public function testEmployeeCannotBreakInAfterCheckout(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 18:00:01',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $data = Attendance::factory()->raw(['type' => AttendanceType::BREAK_IN, 'employee_id' => $this->employee->id]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->create($data);
    }

    public function testEmployeeCannotCheckInMultipleTimesInADay(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 12:00:00',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::now()->toDateString(),
            'employee_id' => $this->employee->id,
        ]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->create($data);
    }

    public function testEmployeePunchCountShouldRestartNextDay(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 12:00:00',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id,
        ]);
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertEquals($data['attend_at'], $result->attend_at);
        $this->assertEquals($data['punch_count'], $result->punch_count);
        $this->assertDatabaseHas('attendances', $data);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository = new AttendanceRepository(new Attendance(), new Setting());
    }
}
