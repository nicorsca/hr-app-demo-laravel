<?php

namespace Attendance;

use App\Modules\Attendances\Database\Models\Attendance;
use App\Modules\Attendances\Database\Models\AttendanceView;
use App\Modules\Attendances\Database\Models\EmployeeView;
use App\Modules\Attendances\Database\Models\Holiday;
use App\Modules\Attendances\Database\Models\Setting;
use App\Modules\Attendances\Repositories\AttendanceReportRepository;
use App\System\Attendance\Foundation\AttendanceType;
use App\System\Employee\Database\Models\Employee;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class AttendanceListUnitTest extends TestCase
{
    private Employee $employee;

    private AttendanceReportRepository $repository;

    public function testAttendanceShouldList(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->timestamp * 1000,
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::now()->unix() * 1000,
            'employee_id' => Employee::factory()->forUser()->forDepartment()->create()
        ]);

        $result = $this->repository->getList(['keyword' => '', 'employee_id' => '', 'start_date' => '', 'end_date' => '', 'on_leave' => '']);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(2, $result);
    }

    public function testAttendanceListShouldBeEmpty(): void
    {
        $result = $this->repository->getList();
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(0, $result);
    }

    public function testAttendanceListShouldBeFilterByEmployeeId(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->timestamp * 1000,
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::now()->unix() * 1000,
            'employee_id' => Employee::factory()->forUser()->forDepartment()->create()
        ]);

        $result = $this->repository->getList(['employee_id' => 1]);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(1, $result);
    }

    public function testAttendanceListShouldBeFilterAttendancesWhoAreOnLeave(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->timestamp * 1000,
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->unix() * 1000,
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::LEAVE_IN,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::now()->unix() * 1000,
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::LEAVE_OUT,
        ]);

        $result = $this->repository->getList(['on_leave' => 1]);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(1, $result);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository = new AttendanceReportRepository(new AttendanceView(), new EmployeeView(), new Holiday(), new Setting());
    }
}
