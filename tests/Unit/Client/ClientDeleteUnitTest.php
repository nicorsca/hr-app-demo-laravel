<?php

namespace Client;

use App\Modules\Clients\Database\Models\Client;
use App\Modules\Clients\Repositories\ClientRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Exceptions\ResourceInUseException;
use Tests\TestCase;

class ClientDeleteUnitTest extends TestCase
{

    /**
     * @var \App\Modules\Clients\Repositories\ClientRepository
     */
    private ClientRepository $repository;

    public function testClientShouldBeDeleted(): void
    {
        $client = Client::factory()->create();
        $result = $this->repository->destroy($client->id);
        $this->assertTrue($result);
        $this->assertNotNull($client->fresh()->deleted_at);
    }

    public function testClientShouldNotBeDeletedIfUsedInProject(): void
    {
        $client = Client::factory()->hasProjects()->create();
        $this->expectException(ResourceInUseException::class);
        $this->repository->destroy($client->id);
    }

    public function testClientShouldNotBeDeletedForUnknownClient(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroy(random_int(1000, 2000));
    }

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ClientRepository(new Client());
    }
}
