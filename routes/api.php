<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\ChangePasswordController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\FileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', '\App\Http\Controllers\Auth\LoginController@authenticate');
    Route::put('change-password', [ChangePasswordController::class, 'changePassword'])
        ->middleware(['auth:api', 'bindings', 'throttle:api', 'userVerified']);

    Route::group(['middleware' => 'authApi'], function () {
        Route::get('me', [AuthController::class, 'getAuthUser']);
        Route::get('me/details', [AuthController::class, 'getAuthEmployeeDetail']);
        Route::put('me/avatar', [AuthController::class, 'updateAvatar']);
        Route::get('logout', [LogoutController::class, 'logout']);
    });
});

Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');

Route::group(['middleware' => ['authApi']], function () {
    Route::post('files/upload', [FileController::class, 'uploadSingleFile']);
    Route::post('files/upload/multiple', [FileController::class, 'uploadMultipleFiles']);
});

