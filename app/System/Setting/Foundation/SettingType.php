<?php


namespace App\System\Setting\Foundation;


class SettingType
{
    public const INTEGER = 'integer';

    public const STRING = 'string';

    public const DATE = 'date';

    public const DATETIME = 'datetime';

    public const ARRAY = 'array';

    public const BOOLEAN = 'boolean';
}
