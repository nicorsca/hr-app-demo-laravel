<?php

namespace App\System\Attendance\Database\Models;

use App\System\Employee\Database\Models\EmployeeView;
use App\System\Foundation\Scope\AddEmployeeScopeTrait;
use App\System\Foundation\Scope\GlobalEmployeeScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use NicoSystem\Foundation\Database\BaseModel;

class AttendanceView extends BaseModel
{
    use AddEmployeeScopeTrait;

    protected $table = 'vw_attendances';

    protected string $defaultSortColumn = 'attend_date';

    protected string $defaultSortOrder = 'desc';

    protected array $sortableColumns = ['attend_date', 'check_in'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->globalQueryName='employee_relation';
    }

    /**
     * @param int|null $unixTimestamp
     *
     * @return string|null
     */
    public function getCheckInAttribute(?int $unixTimestamp): string|null
    {
        if (!is_null($unixTimestamp)) {
            return gmdate("Y-m-d H:i:s", $unixTimestamp);
        }
        return null;
    }

    /**
     * @param int|null $unixTimestamp
     *
     * @return string|null
     */
    public function getCheckOutAttribute(?int $unixTimestamp): string|null
    {
        if (!is_null($unixTimestamp)) {
            return gmdate("Y-m-d H:i:s", $unixTimestamp);
        }
        return null;
    }

    /**
     * @param int|null $unixTimestamp
     *
     * @return string|null
     */
    public function getLeaveInAttribute(?int $unixTimestamp): string|null
    {
        if (!is_null($unixTimestamp)) {
            return gmdate("Y-m-d H:i:s", $unixTimestamp);
        }
        return null;
    }

    /**
     * @param int|null $unixTimestamp
     *
     * @return string|null
     */
    public function getLeaveOutAttribute(?int $unixTimestamp): string|null
    {
        if (!is_null($unixTimestamp)) {
            return gmdate("Y-m-d H:i:s", $unixTimestamp);
        }
        return null;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $startDate
     * @param string $endDate
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereAttendDateLies(Builder $query, string $startDate, string $endDate): Builder
    {
        return $query->whereDate('attend_date', '>=', $startDate)
            ->whereDate('attend_date', '<=', $endDate);
    }

    /**
     * @return BelongsTo
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(EmployeeView::class, 'employee_id', 'id');
    }

    public function breaks(): HasMany
    {
        return $this->hasMany(AttendanceBreakView::class, 'attend_date', 'attend_date');
    }
}
