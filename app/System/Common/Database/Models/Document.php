<?php


namespace App\System\Common\Database\Models;


use App\System\AppBaseModel;
use Database\Factories\DocumentFactory;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use NicoSystem\Foundation\Status;

class Document extends AppBaseModel
{
    /**
     * @var string[]
     */
    protected $fillable = ['title', 'url', 'type', 'thumbnail', 'status', 'documentary_id', 'documentary_type'];

    /**
     * @var array|string[]
     */
    protected array $sortableColumns = ['title', 'created_at', 'status'];

    /**
     * Document constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @var array
     */
    protected $attributes = [
        'status' => Status::STATUS_PUBLISHED
    ];

    /**
     * @return \Database\Factories\DocumentFactory
     */
    protected static function newFactory(): DocumentFactory
    {
        return DocumentFactory::new();
    }

    /**
     * @return MorphTo
     */
    public function documentary(): MorphTo
    {
        return $this->morphTo();
    }

}
