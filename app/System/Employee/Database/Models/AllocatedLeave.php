<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 1/8/2017
 * Time: 2:13 PM
 */

namespace App\System\Employee\Database\Models;

use App\System\AppBaseModel;
use App\System\Setting\Database\Traits\FiscalYearTrait;
use Database\Factories\AllocatedLeaveFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AllocatedLeave extends AppBaseModel
{
    use FiscalYearTrait;

    /**
     * @var array
     */
    protected $fillable = ['title', 'days', 'start_date', 'end_date', 'status', 'type', 'employee_id'];

    /**
     * Leave constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @var array
     */
    protected array $sortableColumns = ['title', 'days', 'start_date', 'end_date', 'created_at', 'status'];

    /**
     * @return \Database\Factories\AllocatedLeaveFactory
     */
    protected static function newFactory(): AllocatedLeaveFactory
    {
        return AllocatedLeaveFactory::new();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }
}
