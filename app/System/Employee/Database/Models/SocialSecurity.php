<?php


namespace App\System\Employee\Database\Models;


use App\System\AppBaseModel;
use Database\Factories\SocialSecurityFactory;

class SocialSecurity extends AppBaseModel
{
    /**
     * @var string[]
     */
    protected $fillable = ['title', 'number', 'start_date', 'end_date', 'status', 'employee_id'];

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'title';

    /**
     * @var array|string[]
     */
    protected array $sortableColumns = ['title', 'number', 'start_date', 'created_at', 'status'];

    /**
     * SocialSecurity constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Database\Factories\SocialSecurityFactory
     */
    protected static function newFactory(): SocialSecurityFactory
    {
        return SocialSecurityFactory::new();
    }
}
