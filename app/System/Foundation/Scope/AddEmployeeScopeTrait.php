<?php

namespace App\System\Foundation\Scope;

trait AddEmployeeScopeTrait
{
    /**
     * @var string $globalQueryName global query name
     */
    protected string $globalQueryName = '';

    /**
     *
     */
    protected static function bootAddEmployeeScopeTrait(): void
    {
        static::addGlobalScope(new GlobalEmployeeScope(static::getGlobalQuery()));
    }

    /**
     * Get GlobalQuery
     */
    public static function getGlobalQuery(): Query
    {
        $static    = new static();
        $queryName = $static->globalQueryName;
        if ($queryName == '') {
            $queryName = $static->getTable();
        }
        return SystemQueryScope::getQueryInstance($queryName);
    }
}
