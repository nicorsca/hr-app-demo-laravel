<?php

namespace App\System;

use Illuminate\Http\Response;

class AppConstants
{
    /**
     * The general constant value for most of the success response
     *
     * @type string
     */
    const SUCCESS_OK = 'ok';

    const ERR_CODE_ZERO = 0;

    const ERR_CODE_404 = 404;

    const UNPROCESSABLE_ENTITY = 422;

    /**
*
 * Note that this constant group is made as per \Illuminate\Contracts\Auth\PasswordBroker constant. We've only replaced dot with an underscore
*/
    const SUCCESS_PASSWORD_RESET_SUCCESS = "passwords_reset";

    const SUCCESS_PASSWORD_RESET_LINK_SENT = "passwords_sent";

    const ERR_PASSWORD_INVALID_USER = "passwords_user";

    const ERR_PASSWORD_INVALID_PASSWORD = "passwords_password";

    const ERR_PASSWORDS_TOKEN = "passwords_token";

    const ERR_PASSWORD_NOT_UPDATED = "password_not_updated";


    /**
     * Used typically whenever user try to submitForm with invalid username/password combination
     *
     * @type string
     */
    const ERR_INVALID_CREDENTIAL = 'invalid_credentials';
    /**
     * Used typically whenever required value in the request is empty
     *
     * @type string
     */
    const ERR_REQUIRED_FIELDS_EMPTY = 'required_fields_empty';

    const ERR_EMAIL_DOESNT_EXIST_IN_DATABASE = 'email_doesnt_exist_in_system';

    const ERR_MAIL_SERVER_AUTH_OR_CONF = "mail_server_auth_or_conf_error";

    const ERR_FORM_VALIDATION = "form_validation_error";

    const ERR_OLD_PASSWORD_MISMATCH = "old_password_mismatch";

    const ERR_INTERNAL_SERVER_ERROR = "internal_server_error";

    const ERR_UNAUTHORIZED = "unauthorized";

    const ERR_FORBIDDEN = "forbidden";

    const ERR_NOT_FOUND = "not_found";

    const ERR_BAD_REQUEST = "err_bad_request";

    const ERR_METHOD_NOT_ALLOWED = "method_not_allowed";

    const ERR_EXPECTATION_FAILED = "expectation_failed";

    const ERR_PRECONDITION_FAILED = "precondition_failed";

    const ERR_CONFLICT = "conflict";

    const ERR_DUPLICATE_RESOURCE = "err_duplicate_resource";

    const ERR_USER_NOT_LOGGED_IN_OR_TOKEN_ABSENT = "user_not_logged_in_or_token_absent";

    const ERR_INVALID_OAUTH_CLIENT = 'invalid_client';

    const ERR_AUTH_TOKEN_ABSENT = 'auth_token_absent';

    const ERR_INVALID_AUTH_TOKEN = 'invalid_auth_token';

    const ERR_INVALID_CALLBACK_URL = 'invalid_callback_url';

    const ERR_TOKEN_EXPIRED = 'auth_token_expired';

    const ERR_TOO_MANY_LOGIN_ATTEMPT = 'too_many_bad_login_attempt';

    const ERR_RESOURCE_IN_USE = 'err_resource_in_use';

    const ERR_RESOURCE_NOT_FOUND = 'err_resource_not_found';

    const ERR_AUTHENTICATION_ERROR = 'err_authentication_error';

    const ERR_SUSPENDED_MODEL_NOT_EDITABLE = "err_resource_not_editable";

    const ERR_USER_NOT_REGISTERED = 'user_not_registered';

    const EMPLOYEE_EXISTS_EXCEPTION = 'employee_exists_exception';

    const PASSWORDS_THROTTLED = 'passwords_throttled';

    const RESOURCE_ALREADY_EXISTS = 'resource_already_exists';

    const USER_DEACTIVATED = 'user_deactivated';

    const EMPLOYEE_ALREADY_HOD = 'employee_already_hod';

    const ERR_LEAVE_EXISTS_EXCEPTION = 'leave_exists_exception';

    const ERR_ALLOCATED_LEAVE_NOT_FOUND = 'allocated_leave_not_found';

    const ERR_PAID_LEAVE_EXCEED_EXCEPTION = 'paid_leave_exceed_exception';

    const ERR_CANNOT_LEAVE_REQUEST_SELF = 'err_cannot_leave_request_self';

    const ERR_ADDRESS_EXISTS_EXCEPTION = 'err_address_exists_exception';

    const ERR_BANK_EXISTS_EXCEPTION = 'err_bank_exists_exception';

    const ERR_EMPLOYEE_ALREADY_DEACTIVATED = 'err_employee_already_deactivated';

    const ERR_EMPLOYEE_ALREADY_ACTIVATED = 'err_employee_already_activated';

    const ERR_EMPLOYEE_EXISTS_EXCEPTION = 'err_employee_exists_exception';

    const ERR_INVALID_LEAVE_STATUS_EXCEPTION = 'err_invalid_leave_status_exception';

    const ERR_CANNOT_CHANGE_LEAVE_STATUS = 'err_cannot_change_leave_status';

    const ERR_LEAVE_EXPIRED_EXCEPTION = 'err_leave_expired_exception';

    const ERR_ALLOCATED_LEAVE_NOT_UPDATABLE = 'err_allocated_leave_not_updatable';

    const ERR_EMPLOYEE_ON_LEAVE = 'err_employee_on_leave';

    const ERR_ATTENDANCE_TYPE_INVALID = 'err_attendance_type_invalid';

    const ERR_ATTENDANCE_ALREADY_CHECKED_IN = 'err_attendance_already_checked_in';

    const ERR_ATTENDANCE_UPDATE_DISABLE = 'err_attendance_update_disable';

    const ERR_ATTENDANCE_UPDATE_TIME_EXCEED_EXCEPTION = 'err_attendance_update_time_exceed_exception';

    const ERR_ATTENDANCE_INTERSECT_EXCEPTION = 'err_attendance_intersect_exception';

    const ERR_ATTENDANCE_SAME_TIME_ATTEND_AT_EXCEPTION = 'err_same_time_attended_exception';

    const ERR_ATTENDANCE_DIFFERENT_DATE_EXCEPTION = 'err_different_attendance_date_exception';

    const ERR_ATTENDANCE_MAX_UPDATED_EXCEPTION = 'err_attendance_max_updated_exception';

    const ERR_HOLIDAY_PASSED = 'err_holiday_passed';

    const ERR_EMPLOYEE_DEACTIVATED_EXCEPTION = 'err_employee_deactivate_exception';

    const ERR_CLIENT_HAS_PROJECT = 'err_client_has_project';

    const ERR_PROJECT_ARCHIVED_EXCEPTION = 'err_project_archived_exception';

    /**
     * @param  $code
     * @return int|string
     */
    public static function getAppMsgCodeFromStatusCode($code): int|string
    {
        switch ($code) {
        case 400:
            return static::ERR_BAD_REQUEST;
        case 401:
            return static::ERR_UNAUTHORIZED;
        case 403:
            return static::ERR_FORBIDDEN;
        case 404:
            return static::ERR_NOT_FOUND;
        case Response::HTTP_METHOD_NOT_ALLOWED:
            return static::ERR_METHOD_NOT_ALLOWED;
        case Response::HTTP_EXPECTATION_FAILED:
            return static::ERR_EXPECTATION_FAILED;
        case Response::HTTP_PRECONDITION_FAILED:
            return static::ERR_PRECONDITION_FAILED;
        case Response::HTTP_INTERNAL_SERVER_ERROR:
            return static::ERR_INTERNAL_SERVER_ERROR;
        default:
            return static::ERR_CODE_ZERO;
        }
    }
}
