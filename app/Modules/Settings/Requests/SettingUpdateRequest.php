<?php

namespace App\Modules\Settings\Requests;

use App\Modules\Settings\Database\Models\Setting;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Foundation\SettingKey;
use App\System\UserPreference\Foundation\UserPreferenceKey;
use App\System\UserPreference\Foundation\UserPreferenceValue;
use NicoSystem\Requests\NicoRequest;

/**
 * Class SettingUpdateRequest
 *
 * @package App\Modules\Settings\Requests
 */
class SettingUpdateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        $id = $this->route('setting');
        $setting = Setting::find($id);

        return $this->getSettingValidationRules($setting?->key);
    }

    /**
     * @param string|null $key
     * @return string[]
     */
    private function getSettingValidationRules(string $key = null): array
    {
        return match ($key) {
            SettingKey::LEAVES_TYPE => [
                'value' => 'required|array',
                'value.*' => 'required|string|max:20|distinct|not_in:' . LeaveType::UNPAID_LEAVE . ',' . LeaveType::CANCELLATION_LEAVE,
            ],
            SettingKey::LEAVES_MAX_REQUEST_DAYS,
            SettingKey::LEAVES_CAN_REQUEST_BEFORE_DAYS,
            SettingKey::ATTENDANCE_EMPLOYEE_UPDATE_BEFORE_DAYS => ['value' => 'required|integer|min:0|max:30'],
            SettingKey::SYSTEM_FISCAL_DATE_START => ['value' => 'required|date_format:m-d'],
            SettingKey::ATTENDANCE_EMPLOYEE_UPDATE_ENABLE => ['value' => 'required|boolean'],
            SettingKey::SYSTEM_WEEKENDS => [
                'value' => 'required|array',
                'value.*' => 'required|string|in:0,1,2,3,4,5,6|distinct',
            ],
            UserPreferenceKey::USER_LIST_VIEW => [
                'value' => 'required|string|in:' . implode(',', UserPreferenceValue::listViewOptions()),
            ],
            UserPreferenceKey::USER_THEME => [
                'value' => 'required|string|in:' . implode(',', UserPreferenceValue::themeOptions()),
            ],
            UserPreferenceKey::USER_TIMEZONE => [
                'value' => 'required|string|max:30',
            ],
            UserPreferenceKey::EMPLOYEE_WORK_TIME => [
                'value' => 'required|array',
                'value.start_work_time' => 'required|date_format:H:i',
                'value.end_work_time' => 'required|date_format:H:i|after:value.start_work_time',
            ],
            SettingKey::DEFAULT_EMAIL_GROUPS => [
                'value' => 'required|array',
                'value.*' => 'required|email:rfc,dns'
            ],
            SettingKey::PROJECT_TYPE => [
                'value' => 'required|array|min:1',
                'value.*' => 'required|string'
            ],
            default => ['value' => 'required'],
        };
    }
}
