<?php


namespace App\Modules\Departments\Exceptions;


use NicoSystem\Exceptions\NicoException;

class HodExistException extends NicoException
{
    /**
     * @var int
     */
    protected $code = 409;

    /**
     * @var string
     */
    protected string $respCode = 'employee_already_hod';

}
