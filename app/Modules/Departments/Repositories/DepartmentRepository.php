<?php

namespace App\Modules\Departments\Repositories;

use App\Events\Department\UpdateHod;
use App\Modules\Departments\Database\Models\Department;
use App\Modules\Departments\Events\Deleting;
use App\Modules\Departments\Events\Updating;
use App\Modules\Departments\Exceptions\HodExistException;
use App\Modules\Departments\Filters\DepartmentsFilter;
use App\Modules\Departments\Interfaces\DepartmentInterface;
use Illuminate\Database\Eloquent\Builder;
use NicoSystem\Repositories\BaseRepository;

/**
 * Class DepartmentRepository
 *
 * @package App\Modules\Departments\Repositories
 */
class DepartmentRepository extends BaseRepository implements DepartmentInterface
{
    /**
     * @var array|string[]
     */
    protected array $events = [
        'updating' => Updating::class,
        'deleting' => Deleting::class,
    ];

    /**
     * DepartmentRepository constructor.
     *
     * @param Department $model
     */
    public function __construct(Department $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     * @return DepartmentsFilter
     */
    public function getFilter(Builder $builder): DepartmentsFilter
    {
        return new DepartmentsFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->with(
            [
                'hod' => function ($query) {
                    $query->select(['id', 'name', 'position', 'avatar', 'department_id']);
                }]
        );
    }

    /**
     * @param string $id
     * @param array $attributes
     * @return \App\Modules\Departments\Database\Models\Department
     */
    public function getById(string $id, array $attributes = []): Department
    {
        return $this->model->with(
            [
                'hod' => function ($query) {
                    $query->select(['id', 'name', 'position', 'avatar', 'department_id']);
                },
                'employees' => function ($query) {
                    $query->select(['id', 'name', 'position', 'avatar', 'email', 'department_id']);
                }]
        )
            ->findOrFail($id);
    }

    /**
     * @param string $departmentId
     * @param string|null $employeeId
     * @return void
     */
    public function updateHod(string $departmentId, string|null $employeeId): void
    {
        $department = $this->model->findOrfail($departmentId);
        $oldHodId = $department->employee_id;
        if ($oldHodId == $employeeId) {
            return;
        }
        if ($employeeId) {
            $hod = $this->model->where('employee_id', $employeeId)->first();
            if ($hod) {
                throw new HodExistException(trans('responses.department.employee_already_hod'));
            }
        }
        $department->employee_id = $employeeId;
        $department->save();
        event(new UpdateHod($department, $oldHodId, $employeeId));
    }
}
