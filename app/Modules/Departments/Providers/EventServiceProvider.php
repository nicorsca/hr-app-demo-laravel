<?php


namespace App\Modules\Departments\Providers;

use App\Modules\Departments\Listeners\DepartmentEventSubscriber;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        DepartmentEventSubscriber::class,
    ];
}
