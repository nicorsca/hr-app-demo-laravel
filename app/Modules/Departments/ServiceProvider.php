<?php
namespace App\Modules\Departments;

use App\Modules\Departments\Providers\EventServiceProvider;
use \Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 *
 * @package App\Modules\Departments
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected $defer = true;

    /**
     * Register your binding
     */
    public function register()
    {
        $this->app->bind('App\Modules\Departments\Interfaces\DepartmentInterface', 'App\Modules\Departments\Repositories\DepartmentRepository');
        $this->app->register(EventServiceProvider::class);
    }
}
