<?php
namespace App\Modules\Attendances;

use App\Modules\Attendances\Interfaces\AttendanceExcelReportEmployeesSummaryInterface;
use App\Modules\Attendances\Providers\EventServiceProvider;
use App\Modules\Attendances\Repositories\AttendanceExcelReportEmployeesSummaryRepository;
use \Illuminate\Support\ServiceProvider as BaseServiceProvider;
use App\Modules\Attendances\Interfaces\AttendanceInterface;
use App\Modules\Attendances\Repositories\AttendanceRepository;
use App\Modules\Attendances\Interfaces\AttendanceReportInterface;
use App\Modules\Attendances\Repositories\AttendanceReportRepository;

/**
 * Class ServiceProvider
 * @package App\Modules\Attendances
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected $defer = true;

    /**
     * Register your binding
     */
    public function register(): void
    {
        $this->app->bind(AttendanceInterface::class, AttendanceRepository::class);
        $this->app->bind(AttendanceReportInterface::class, AttendanceReportRepository::class);
        $this->app->bind(AttendanceExcelReportEmployeesSummaryInterface::class, AttendanceExcelReportEmployeesSummaryRepository::class);
        $this->app->register(EventServiceProvider::class);
    }
}
