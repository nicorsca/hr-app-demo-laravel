<?php

namespace App\Modules\Attendances\Repositories;

use App\Modules\Attendances\Database\Models\AttendanceView;
use App\Modules\Attendances\Database\Models\EmployeeView;
use App\Modules\Attendances\Database\Models\Holiday;
use App\Modules\Attendances\Database\Models\Setting;
use App\Modules\Attendances\Filters\AttendanceReportFilter;
use App\Modules\Attendances\Interfaces\AttendanceReportInterface;
use App\System\Attendance\Foundation\AttendanceType;
use App\System\Setting\Foundation\SettingDefaultValue;
use App\System\Setting\Foundation\SettingKey;
use App\System\UserPreference\Foundation\UserPreferenceKey;
use App\System\UserPreference\Foundation\UserPreferenceValue;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use NicoSystem\Repositories\BaseRepository;

class AttendanceReportRepository extends BaseRepository implements AttendanceReportInterface
{
    /**
     * @var EmployeeView|null
     */
    private ?EmployeeView $globalEmployee = null;

    /**
     * @param AttendanceView $model
     * @param EmployeeView $employee
     * @param Holiday $holiday
     * @param Setting $setting
     */
    public function __construct(
        AttendanceView $model,
        private EmployeeView $employee,
        private Holiday $holiday,
        private Setting $setting
    ) {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     *
     * @return AttendanceReportFilter
     */
    public function getFilter(Builder $builder): AttendanceReportFilter
    {
        return new AttendanceReportFilter($builder);
    }

    /**
     * @param Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->with(['employee' => function(BelongsTo $query) {
            $query->select('id', 'name', 'avatar');
        }]);
        if (!is_null($this->globalEmployee)) {
            $builder->with([
                'breaks' => function ($query) {
                    $query->where('employee_id', $this->globalEmployee->id);
                }
            ]);
        }
    }

    /**
     * @param array $inputs
     * @param string $employeeId
     *
     * @return array
     */
    public function getEmployeeAttendanceCalendar(array $inputs, string $employeeId): array
    {
        $this->globalEmployee  = $this->employee->findOrfail($employeeId);
        $inputs['employee_id'] = $employeeId;
        $attendances           = parent::getList($inputs, false);

        return $this->getEachDatesReport($inputs, $attendances, [
            'check_in',
            'check_out',
            'leave_in',
            'leave_out',
            'office_time',
            'work_time',
            'break_time',
            'leave_time',
            'breaks'
        ]);
    }

    /**
     * @param array $inputs
     * @param Collection $attendances
     * @param array $attendResponseKeys
     *
     * @return array
     */
    public function getEachDatesReport(array $inputs, Collection $attendances, array $attendResponseKeys): array
    {
        $holidays         = $this->holiday->whereBetween('date',
            [$inputs['start_date'], $inputs['end_date']])->get();
        $period           = CarbonPeriod::create($inputs['start_date'], $inputs['end_date']);
        $setting          = $this->setting->whereIn('key',
            [SettingKey::SYSTEM_WEEKENDS, SettingKey::ATTENDANCE_EMPLOYEE_WORKING_HOURS])->get();
        $weekends         = $setting->firstWhere('key', SettingKey::SYSTEM_WEEKENDS)?->value ?? [];
        $workingHour      = $setting->firstWhere('key',
                SettingKey::ATTENDANCE_EMPLOYEE_WORKING_HOURS)?->value ?? SettingDefaultValue::ATTENDANCE_EMPLOYEE_WORKING_HOURS_VALUE;
        $result           = [];
        $totalWorkingDays = 0;
        $sumOfficeTime    = 0;
        $officeTime       = in_array('avg_office_time', $attendResponseKeys, false) ? 'avg_office_time' : 'office_time';

        foreach ($period as $date) {
            $dateFormat = $date->format('Y-m-d');
            $attend     = $attendances->firstWhere('attend_date', $dateFormat)?->only($attendResponseKeys);
            if (is_null($attend)) {
                $attend = array_fill_keys($attendResponseKeys, null);
            }
            $extra            = [
                'date'    => $dateFormat,
                'weekend' => in_array($date->dayOfWeek, $weekends, false),
                'holiday' => (bool)$holidays->firstWhere('date', $dateFormat),
            ];
            $totalWorkingDays += !($extra['weekend'] || $extra['holiday']);
            $sumOfficeTime    += $attend[$officeTime];
            $result[]         = array_merge($extra, $attend);
        }

        return array_merge(['data' => $result], [
            'total' => [
                'sum_office_hr'  => $sumOfficeTime / 3600,
                'actual_work_hr' => $totalWorkingDays * $workingHour
            ]
        ]);
    }

    /**
     * @param array $inputs
     * @param string $employeeId
     *
     * @return AttendanceView
     */
    public function getEmployeeAttendanceSummary(array $inputs, string $employeeId): AttendanceView
    {
        $this->employee->findOrfail($employeeId);

        return $this->model
            ->selectRaw("sum(case when check_in is not null then 1 else 0 end) as present_days,
            sum(case when leave_in is not null then 1 else 0 end) as leave_days,
            sum(office_time) as total_office_time,
            sum(work_time) as total_work_time,
            sum(break_time) as total_break_time,
            sum(leave_time) as total_leave_time")
            ->whereAttendDateLies($inputs['start_date'], $inputs['end_date'])
            ->where('employee_id', $employeeId)
            ->first();
    }

    /**
     * @param array $inputs
     * @param string $groupBy
     *
     * @return Collection
     */
    public function getEmployeesReport(array $inputs, string $groupBy): Collection
    {
        $builder = $this->model->select($groupBy)
            ->selectRaw("sum(case when check_in is not null then 1 else 0 end) as present_days,
            sum(case when leave_in is not null then 1 else 0 end) as leave_days,
            SEC_TO_TIME(AVG(TIME_TO_SEC(FROM_UNIXTIME(check_in,'%H:%i:%s')))) as avg_check_in,
            SEC_TO_TIME(AVG(TIME_TO_SEC(FROM_UNIXTIME(check_out,'%H:%i:%s')))) as avg_check_out,
            sum(office_time) as total_office_time,
            avg(office_time) as avg_office_time,
            sum(work_time) as total_work_time,
            avg(work_time) as avg_work_time,
            sum(break_time) as total_break_time,
            avg(break_time) as avg_break_time
            ")
            ->whereAttendDateLies($inputs['start_date'], $inputs['end_date'])
            ->groupBy($groupBy);

        if (Arr::has($inputs, ['employee_id'])) {
            $builder->where('employee_id', $inputs['employee_id']);
        }
        if ($groupBy === 'employee_id') {
            $builder->with([
                'employee' => function (BelongsTo $query) {
                    $query->select(['id', 'name', 'email', 'avatar', 'position']);
                }
            ]);
        }
        return $builder->orderBy('attend_date', 'asc')->get();
    }

    /**
     * @param array $inputs
     *
     * @return int
     */
    public function getWorkingDays(array $inputs): int
    {
        $holidays      = $this->holiday->whereBetween('date', [$inputs['start_date'], $inputs['end_date']])->get();
        $period        = CarbonPeriod::create($inputs['start_date'], $inputs['end_date']);
        $weekends      = $this->setting->where('key', SettingKey::SYSTEM_WEEKENDS)->first()?->value ?? [];
        $totalHolidays = 0;
        foreach ($period as $date) {
            $totalHolidays += in_array($date->dayOfWeek, $weekends, false) || (bool)$holidays->firstWhere('date',
                    $date->format('Y-m-d'));
        }
        $totalDays = $period->getStartDate()->diffInDays($period->getEndDate()?->addDay());
        return $totalDays - $totalHolidays;
    }

    /**
     * @param array $inputs
     *
     * @return Collection
     */
    public function getTodayAttendance(array $inputs): Collection
    {
        $builder = $this->employee->select('name', 'email', 'avatar', 'vw_employees.id', 'code', 'position',
            'value as employee_work_time',
            'vw_attendances.*',DB::raw('FROM_UNIXTIME(check_in) as check_in'),DB::raw('FROM_UNIXTIME(check_out) as check_out'))
            ->leftjoin('user_preferences', function (JoinClause $join) {
                $join->on('vw_employees.user_id', '=', 'user_preferences.user_id')
                    ->where('key', UserPreferenceKey::EMPLOYEE_WORK_TIME);
            })->leftjoin('vw_attendances', function (JoinClause $join) {
                $join->on('vw_employees.id', '=', 'vw_attendances.employee_id')
                    ->whereDate('vw_attendances.attend_date', Carbon::today()->toDateString());
            })->active();
        if (Arr::has($inputs, 'keyword')) {
            $builder->where('vw_employees.name', 'like', "%{$inputs['keyword']}%");
        }
        return $builder->get();
    }

    /**
     * @return array
     */
    public function getTodaySummary(): array
    {
        $employees = $this->employee->active()->count();
        $present   = $this->employee->whereHas('attendances', function (Builder $query) {
            $query->where('type', AttendanceType::CHECK_IN)
                ->whereDate('attend_at', Carbon::today()->toDateString())
                ->published();
        })->count();
        $leaves    = $this->employee->whereHas('attendances', function (Builder $query) {
            $query->where('type', AttendanceType::LEAVE_IN)
                ->whereDate('attend_at', Carbon::today()->toDateString())
                ->published();
        })->count();

        $workTime           = $this->setting->where('key',
                UserPreferenceKey::EMPLOYEE_WORK_TIME)->first()?->value ?? UserPreferenceValue::EMPLOYEE_WORK_TIME_VALUE;
        $startWorkTime      = convert_hour_minute_to_seconds(explode('-', $workTime)[0]);
        $todayStartWorkTime = Carbon::today()->addSeconds($startWorkTime + 900)->toDateTimeString();

        $onTimeLogin = $this->employee->whereHas('attendances', function (Builder $query) use ($todayStartWorkTime) {
            $query->where('type', AttendanceType::CHECK_IN)
                ->whereAttendAtLies(Carbon::today()->toDateTimeString(), $todayStartWorkTime)
                ->published();
        })->count();

        return [
            'employees'     => $employees,
            'present'       => $present,
            'leaves'        => $leaves,
            'absent'        => $employees - $present - $leaves,
            'on_time_login' => $onTimeLogin,
            'late_login'    => $present - $onTimeLogin,
        ];
    }

    /**
     * @param array $inputs
     *
     * @return Collection
     */
    public function getEmployeesAttendanceCalendar(array $inputs): Collection
    {
        $employees = $this->employee->newQuery()
            ->with([
                'attends' => static function (HasMany $query) use ($inputs) {
                    $query->whereAttendDateLies($inputs['start_date'], $inputs['end_date'])
                        ->orderBy('attend_date', 'desc');
                }
            ])->select('id', 'name', 'avatar')->get();

        $holidays = $this->holiday->whereBetween('date', [$inputs['start_date'], $inputs['end_date']])->get();
        $period   = CarbonPeriod::create($inputs['start_date'], $inputs['end_date']);
        $weekends = $this->setting->where('key', SettingKey::SYSTEM_WEEKENDS)->first()?->value ?? [];

        return $employees->map(function ($employee) use ($holidays, $period, $weekends) {
            $attendances = [];
            foreach ($period as $date) {
                $dateFormat    = $date->format('Y-m-d');
                $attend        = $employee->attends->firstWhere('attend_date', $dateFormat);
                $attendances[] = [
                    'date'    => $dateFormat,
                    'present' => (bool)$attend?->check_in,
                    'leave'   => (bool)$attend?->leave_in,
                    'weekend' => in_array($date->dayOfWeek, $weekends, false),
                    'holiday' => (bool)$holidays->firstWhere('date', $dateFormat),
                ];
            }
            $employee->attendances = $attendances;
            $employee->unsetRelation('attends');
            return $employee;
        });
    }
}
