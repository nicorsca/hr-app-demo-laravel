<?php

namespace App\Modules\Attendances\Repositories;

use App\Modules\Attendances\Database\Models\Attendance;
use App\Modules\Attendances\Database\Models\Setting;
use App\Modules\Attendances\Events\Updated;
use App\Modules\Attendances\Exceptions\AttendanceIntersectException;
use App\Modules\Attendances\Exceptions\AttendanceUpdateDisableException;
use App\Modules\Attendances\Exceptions\AttendanceUpdateTimeExceedException;
use App\Modules\Attendances\Exceptions\DifferentAttendanceDateException;
use App\Modules\Attendances\Exceptions\SameTimeAttendedException;
use App\Modules\Attendances\Filters\AttendancesFilter;
use App\Modules\Attendances\Interfaces\AttendanceInterface;
use App\System\Attendance\Foundation\AttendanceType;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Foundation\Status;
use NicoSystem\Repositories\BaseRepository;

/**
 * Class AttendanceRepository
 * @package App\Modules\Attendances\Repositories
 */
class AttendanceRepository extends BaseRepository implements AttendanceInterface
{
    /**
     * AttendanceRepository constructor.
     *
     * @param Attendance $model
     * @param Setting $setting
     */
    public function __construct(Attendance $model, private Setting $setting)
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     *
     * @return AttendancesFilter
     */
    public function getFilter(Builder $builder): AttendancesFilter
    {
        return new AttendancesFilter($builder);
    }

    /**
     * @param Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->published()->with('parent.parent');
    }

    /**
     * @param array $inputs
     *
     * @return LengthAwarePaginator|Collection
     */
    public function getDetails(array $inputs): LengthAwarePaginator|Collection
    {
        $attendances = $this->getList($inputs, false);

        return $attendances->map(function (Attendance $attendance) {
            $parent              = $attendance->parent?->only('id', 'attend_at', 'reason', 'browser', 'device',
                'location', 'ip');
            $grandParent         = $attendance->parent?->parent?->only('id', 'attend_at', 'reason', 'browser', 'device',
                'location', 'ip');
            $parents             = collect([$parent, $grandParent]);
            $attendance->parents = $parents->filter();
            return $attendance->unsetRelation('parent');
        });
    }

    /**
     * @param array $inputs
     *
     * @return Attendance
     */
    public function create(array $inputs): Attendance
    {
        if ($inputs['type'] == AttendanceType::CHECK_IN) {
            $todayCheckIn = $this->model
                ->where('employee_id', $inputs['employee_id'])
                ->whereDate('attend_at', $inputs['attend_at'])
                ->where('type', AttendanceType::CHECK_IN)
                ->published()
                ->first();
            if ($todayCheckIn) {
                throw new NicoBadRequestException(trans('responses.attendance.already_checked_in'),
                    'err_attendance_already_checked_in');
            }
        }

        $attendance = $this->model
            ->where('employee_id', $inputs['employee_id'])
            ->where('attend_at', '<=', $inputs['attend_at'])
            ->descending('attend_at')
            ->first();
        if (!$this->validationAttendanceType($inputs['type'], $attendance?->type)) {
            throw new NicoBadRequestException(trans('responses.attendance.invalid_type', ['type' => $inputs['type']]),
                'err_attendance_type_invalid');
        }

        $todayPunchIn          = $this->model
            ->where('employee_id', $inputs['employee_id'])
            ->whereDate('attend_at', Carbon::today()->toDateString())
            ->where('type', $inputs['type'])
            ->first();
        $inputs['punch_count'] = $todayPunchIn ? ++$todayPunchIn->punch_count : 1;
        $inputs['status']      = Status::STATUS_PUBLISHED;
        return parent::create($inputs);
    }

    /**
     * @param string $requestedType
     * @param string|null $latestType
     *
     * @return bool
     */
    private function validationAttendanceType(string $requestedType, string $latestType = null): bool
    {
        if (!$latestType) {
            return $requestedType == AttendanceType::CHECK_IN;
        }
        return match ($requestedType) {
            AttendanceType::CHECK_IN => $latestType == AttendanceType::CHECK_OUT | $latestType == AttendanceType::LEAVE_OUT,
            AttendanceType::CHECK_OUT, AttendanceType::BREAK_OUT => $latestType == AttendanceType::CHECK_IN | $latestType == AttendanceType::BREAK_IN,
            AttendanceType::BREAK_IN => $latestType == AttendanceType::BREAK_OUT,
            default => false,
        };
    }

    /**
     * @param Attendance $attendance
     * @param array $inputs
     *
     * @return Attendance
     */
    public function updateAttendanceByEmployee(Attendance $attendance, array $inputs): Attendance
    {
        $settings     = $this->setting->whereIn('key',
            [SettingKey::ATTENDANCE_EMPLOYEE_UPDATE_ENABLE, SettingKey::ATTENDANCE_EMPLOYEE_UPDATE_BEFORE_DAYS])->get();
        $updateEnable = $settings->firstWhere('key', SettingKey::ATTENDANCE_EMPLOYEE_UPDATE_ENABLE);
        if ($updateEnable?->value == 0) {
            throw new AttendanceUpdateDisableException(trans('responses.attendance.employee_update_disable'));
        }
        $updateBeforeDays = $settings->firstWhere('key', SettingKey::ATTENDANCE_EMPLOYEE_UPDATE_BEFORE_DAYS);
        if ($attendance->attend_at <= Carbon::today()->subDays($updateBeforeDays?->value)->subSecond()) {
            throw new AttendanceUpdateTimeExceedException(trans('responses.attendance.update_time_exceed'));
        }

        return $this->updateAttendance($attendance, $inputs);
    }

    /**
     * @param Attendance $attendance
     * @param array $inputs
     *
     * @return Attendance
     */
    public function updateAttendance(Attendance $attendance, array $inputs): Attendance
    {
        if ($attendance->parent?->parent) {
            throw new NicoBadRequestException(trans('responses.attendance.max_updated'),
                'err_attendance_max_updated_exception');
        }
        if ($attendance->attend_at == $inputs['attend_at']) {
            throw new SameTimeAttendedException(trans('responses.attendance.same_time_attend_at'));
        }
        if (Carbon::parse($attendance->attend_at)->toDateString() != Carbon::parse($inputs['attend_at'])->toDateString()) {
            throw new DifferentAttendanceDateException(trans('responses.attendance.different_date'));
        }
        $previousAttendance = $this->model->where('attend_at', '<=', $attendance->attend_at)
            ->where('id', '!=', $attendance->id)
            ->where('employee_id', $attendance->employee_id)
            ->published()
            ->orderBy('attend_at', 'desc')
            ->first();
        $nextAttendance     = $this->model->where('attend_at', '>=', $attendance->attend_at)
            ->where('id', '!=', $attendance->id)
            ->where('employee_id', $attendance->employee_id)
            ->published()
            ->orderBy('attend_at', 'desc')
            ->first();
        $isInDateRange      = check_date_time_is_between_date_range($inputs['attend_at'],
            $previousAttendance?->attend_at, $nextAttendance?->attend_at);
        if (!$isInDateRange) {
            throw new AttendanceIntersectException(trans('responses.attendance.intersect',
                ['previous' => $previousAttendance?->attend_at, 'next' => $nextAttendance?->attend_at]));
        }
        $newAttendance            = $attendance->replicate(['attend_at']);
        $newAttendance->parent_id = $attendance->id;
        $newAttendance->fill($inputs);
        $newAttendance->save();

        $attendance->status = Status::STATUS_UNPUBLISHED;
        $attendance->save();

        event(new Updated($attendance, $newAttendance));

        return $newAttendance;
    }

    /**
     * @param string $id
     *
     * @return Attendance
     */
    public function getAttendanceByIdExceptLeaves(string $id): Attendance
    {
        return $this->model->exceptLeaves()->published()->with('parent.parent')->findOrFail($id);
    }
}
