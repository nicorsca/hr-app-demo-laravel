<?php

namespace App\Modules\Attendances\Requests;

use App\Modules\Attendances\Database\Models\Employee;
use Illuminate\Validation\Rule;
use NicoSystem\Requests\NicoRequest;

/**
 * Class AttendanceUpdateRequest
 * @package App\Modules\Attendances\Requests
 */
class AttendanceUpdateRequest extends NicoRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'attend_at' => 'required|date_format:Y-m-d H:i:s|before_or_equal:now',
            'reason' => 'required|string|max:500',
            'browser' => 'nullable|string|max:50',
            'device' => 'nullable|string|max:100',
            'location' => 'nullable|string|max:100',
            'ip' => 'nullable|ip',
            'fingerprint' => 'nullable|integer',
            'employee_id' => [
                'nullable',
                'integer',
                Rule::exists('vw_employees', 'id')
                    ->where('status', Employee::STATUS_ACTIVE)
                    ->whereNull('deleted_at')
            ]
        ];
    }
}
