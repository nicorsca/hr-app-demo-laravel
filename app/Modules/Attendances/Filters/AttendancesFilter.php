<?php

namespace App\Modules\Attendances\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class AttendancesController
 * @package App\Modules\Attendances\Filters
 */
class AttendancesFilter extends BaseFilter
{
    /**
     * To do filter here
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        $this->type($keyword);
    }

    /**
     * @param string $type
     */
    public function type(string $type = ''): void
    {
        if ($type != '') {
            $this->builder->where('type', $type);
        }
    }

    /**
     * @param string $title
     */
    public function title(string $title = ''): void
    {
        $this->type($title);
    }

    /**
     * @param int|null $employeeId
     */
    public function employee_id(int $employeeId = null): void
    {
        if ($employeeId) {
            $this->builder->where('employee_id', $employeeId);
        }
    }

    /**
     * @param string $date
     */
    public function date(string $date = ''): void
    {
        if ($date != '') {
            $this->builder->whereDate('attend_at', $date);
        }
    }

}
