<?php

namespace App\Modules\Attendances\Providers;

use App\Modules\Attendances\Listeners\AttendanceEventSubscriber;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        AttendanceEventSubscriber::class,
    ];


}
