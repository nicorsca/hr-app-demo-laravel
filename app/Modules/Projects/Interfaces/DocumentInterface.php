<?php

namespace App\Modules\Projects\Interfaces;

use App\Modules\Projects\Database\Models\Document;
use App\System\Common\Database\Models\Document as SystemDocument;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface DocumentInterface
{
    /**
     * @param  array  $params
     * @param  string $projectId
     * @return Collection|LengthAwarePaginator
     */
    public function getDocumentList(array $params, string $projectId): Collection|LengthAwarePaginator;

    /**
     * @param  array  $inputs
     * @param  string $projectId
     * @return Document
     */
    public function createDocument(array $inputs, string $projectId): Document;

    /**
     * @param  array  $inputs
     * @param  string $projectId
     * @param  string $documentId
     * @return Document
     */
    public function updateDocument(array $inputs, string $projectId, string $documentId): Document;

    /**
     * @param  string $projectId
     * @param  string $documentId
     * @return mixed
     */
    public function destroyDocument(string $projectId, string $documentId): bool;

    /**
     * @param  string $projectId
     * @param  string $documentId
     * @return Document
     */
    public function getDocumentById(string $projectId, string $documentId): SystemDocument;
}
