<?php

namespace App\Modules\Projects\Requests;

use NicoSystem\Requests\NicoRequest;

class RoleUpdateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        $id = $this->route('project_role');

        return [
            'title' => "required|string|max:100|unique:project_roles,title,{$id},id,deleted_at,NULL",
        ];
    }
}
