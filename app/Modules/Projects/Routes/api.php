<?php

use App\Modules\Projects\Controllers\ProjectsController;
use App\Modules\Projects\Controllers\MembersController;
use App\Modules\Projects\Middlewares\ArchivedProject;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['authApi', ArchivedProject::class]], function () {
    Route::apiResource('projects', 'ProjectsController')->only('index', 'store', 'update', 'show');
    Route::group(['prefix' => 'projects/{project}'], function () {
        Route::put('archive', [ProjectsController::class, 'archive']);
        Route::apiResource('documents', 'DocumentsController');
        Route::put('assign', [MembersController::class, 'assignMembers']);
        Route::put('revoke', [MembersController::class, 'revokeMember']);
    });
});

Route::group(['middleware' => ['authApi', ArchivedProject::class]], function () {
    Route::apiResource('project-roles', 'RolesController')->only('index', 'store', 'update', 'destroy');
});
