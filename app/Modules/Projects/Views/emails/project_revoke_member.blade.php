
@extends('emails.base_email')

@section('mail_body')
    {!! trans('project_member_revoke.body', compact('title')) !!}
@stop
