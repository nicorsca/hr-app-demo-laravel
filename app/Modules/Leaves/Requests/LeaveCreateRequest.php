<?php

namespace App\Modules\Leaves\Requests;

use App\Modules\Leaves\Database\Models\Employee;
use App\Modules\Leaves\Database\Models\Setting;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use NicoSystem\Requests\NicoRequest;

/**
 * Class LeaveCreateRequest
 *
 * @package App\Modules\Leaves\Requests
 */
class LeaveCreateRequest extends NicoRequest
{

    /**
     * @return array
     */
    public function rules(): array
    {
        $settings = Setting::whereIn('key', [SettingKey::LEAVES_TYPE, SettingKey::LEAVES_MAX_REQUEST_DAYS])->get();

        $type = $settings->firstWhere('key', SettingKey::LEAVES_TYPE);
        $type = array_merge([LeaveType::UNPAID_LEAVE], $type?->value ?? []);
        $maxLeaves = $settings->firstWhere('key', SettingKey::LEAVES_MAX_REQUEST_DAYS);
        $beforeDate = Carbon::parse($this->request->get('start_at'))->addDays($maxLeaves?->value)->subSecond()->toDateTimeString();
        $afterDate = Carbon::today()->addDay()->toDateTimeString();
        if ($this->has('is_emergency') && $this->is_emergency == true) {
            $afterDate = Carbon::today()->subWeeks(2)->toDateTimeString();
        }

        return [
            'title' => 'required|string|max:100',
            'description' => 'required|string',
            'start_at' => 'required|date_format:Y-m-d H:i:s|after_or_equal:' . $afterDate,
            'end_at' => 'required|date_format:Y-m-d H:i:s|after:start_at|before_or_equal:' . $beforeDate,
            'type' => 'required|in:' . implode(',', $type),
            'is_emergency' => 'required|boolean',
            'requested_to' => [
                'required',
                'integer',
                Rule::exists('vw_employees', 'id')
                    ->where('status', Employee::STATUS_ACTIVE)
                    ->whereNull('deleted_at')
            ]
        ];
    }
}
