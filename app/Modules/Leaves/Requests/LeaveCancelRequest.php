<?php


namespace App\Modules\Leaves\Requests;


use NicoSystem\Requests\NicoRequest;

class LeaveCancelRequest extends NicoRequest
{

    public function rules(): array
    {
        return [
            'reason' => 'required|string',
        ];
    }
}
