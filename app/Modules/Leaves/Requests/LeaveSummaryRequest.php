<?php

namespace App\Modules\Leaves\Requests;

use Illuminate\Validation\Rule;
use NicoSystem\Requests\NicoRequest;

class LeaveSummaryRequest extends NicoRequest
{

    public function rules(): array
    {
        return [
            'employee_id' =>  [
                'required',
                'integer',
                Rule::exists('employees', 'id')
                    ->whereNull('deleted_at')
            ]
        ];
    }
}
