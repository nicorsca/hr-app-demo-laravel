<?php

namespace App\Modules\Leaves\Listeners;

use App\Events\Leave\ApproveLeaveRequest;
use App\Modules\Leaves\Events\CancelLeaveRequest;
use App\Modules\Leaves\Events\Created;
use App\Modules\Leaves\Events\Updated;
use App\Modules\Leaves\Mails\ApproveLeaveRequestMail;
use App\Modules\Leaves\Mails\CancelLeaveRequestMail;
use App\Modules\Leaves\Mails\LeaveRequestMail;
use App\Modules\Leaves\Mails\LeaveRequestUpdateMail;
use App\System\Foundation\Scope\GlobalEmployeeScope;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Mail;

class LeaveEventSubscriber
{
    /**
     * @param Created $event
     */
    public function onLeaveCreated(Created $event): void
    {
        $leave = $event->leave;

        if ($leave->created_by == $leave->employee->user_id) {
            Mail::to($leave->requested->email)->queue(new LeaveRequestMail($leave));
        }
    }

    /**
     * @param \App\Modules\Leaves\Events\Updated $event
     */
    public function onLeaveUpdated(Updated $event): void
    {
        $leave = $event->leave;
        Mail::to($leave->requested->email)->queue(new LeaveRequestUpdateMail($leave));
    }

    /**
     * @param CancelLeaveRequest $event
     */
    public function onCancelLeaveRequest(CancelLeaveRequest $event): void
    {
        $authUser = $event->authUser;
        $leave = $event->leave;
        if ($authUser->id == $leave->employee->user_id) {
            $receiver = $leave->requested;
            $sender = $leave->employee;
        } else {
            $receiver = $leave->employee;
            $sender = $leave->requested;
        }
        Mail::to($receiver->email)->queue(new CancelLeaveRequestMail($event->leave, $receiver, $sender));
    }

    /**
     * @param ApproveLeaveRequest $event
     */
    public function onApproveLeaveRequest(ApproveLeaveRequest $event): void
    {
        $leave = $event->leave;
        Mail::to($leave->employee->email)->queue(new ApproveLeaveRequestMail($leave));
    }

    /**
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events): void
    {
        $events->listen(Created::class, [LeaveEventSubscriber::class, 'onLeaveCreated']);
        $events->listen(Updated::class, [LeaveEventSubscriber::class, 'onLeaveUpdated']);
        $events->listen(CancelLeaveRequest::class, [LeaveEventSubscriber::class, 'onCancelLeaveRequest']);
        $events->listen(ApproveLeaveRequest::class, [LeaveEventSubscriber::class, 'onApproveLeaveRequest']);
    }
}
