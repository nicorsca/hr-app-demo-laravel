<?php


namespace App\Modules\Leaves\Database\Models;

use App\System\Employee\Database\Models\AllocatedLeave as BaseModel;
use Illuminate\Database\Eloquent\Builder;

class AllocatedLeave extends BaseModel
{

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string                                $startDate
     * @param  string                                $endDate
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublishedBetweenStartAndEndDate(Builder $query, string $startDate, string $endDate): Builder
    {
        return $query->whereDate('start_date', '>=', $startDate)
            ->whereDate('end_date', '<=', $endDate)->published();
    }

}
