<?php


namespace App\Modules\Leaves\Exceptions;


use NicoSystem\Exceptions\NicoException;

class AllocatedLeaveNotFoundException extends NicoException
{

    /**
     * @var int
     */
    protected $code = 404;

    /**
     * @var string
     */
    protected string $respCode = 'allocated_leave_not_found';
}
