<?php

use App\Modules\Leaves\Controllers\LeavesController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['authApi']], function () {
    Route::post('leaves/emergency', [LeavesController::class, 'storeEmergencyLeaveRequest']);
    Route::put('leaves/{leave}/cancel', [LeavesController::class, 'cancelLeaveRequest']);
    Route::put('leaves/{leave}/decline', [LeavesController::class, 'declineLeaveRequest'])->middleware('role:manager');
    Route::put('leaves/{leave}/approve', [LeavesController::class, 'approveLeaveRequest'])->middleware('role:manager');
    Route::post('leaves/{leave}/cancellation', [LeavesController::class, 'storeRequestCancellationLeave']);
    Route::get('leaves/summary', [LeavesController::class, 'getSummary']);
    Route::resource('leaves', 'LeavesController')->only('index', 'store', 'show', 'update');
});
