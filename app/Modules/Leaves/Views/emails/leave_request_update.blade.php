<!DOCTYPE html>
<html>
<head>
    <title>{{trans('base_email.title',['title'=> 'Updated: ' . $title])}}</title>
</head>

<body>
{!! trans('base_email.salutation',['receiverName' => $receiver ]) !!}

<p>
    {!! $sender !!}, requested leave for a following reason:
</p>
<p>
    {!! $description !!}
</p>
<p><strong>Type:</strong> {!! $type !!}</p>
<p><strong>Time period:</strong> {!! $start_at !!} - {!! $end_at !!}</p>
<p><strong>Days:</strong> {!! $days !!}</p>
<p><strong>Is emergency:</strong> {!! $is_emergency ? "true" : "false" !!}</p>

</body>
</html>


