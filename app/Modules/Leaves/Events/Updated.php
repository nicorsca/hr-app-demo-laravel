<?php

namespace App\Modules\Leaves\Events;

use App\Modules\Leaves\Database\Models\Leave;

class Updated
{
    /**
     * @param \App\Modules\Leaves\Database\Models\Leave $leave
     */
    public function __construct(public Leave $leave)
    {
    }
}
