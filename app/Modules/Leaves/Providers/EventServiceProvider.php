<?php

namespace App\Modules\Leaves\Providers;

use App\Modules\Leaves\Listeners\LeaveEventSubscriber;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        LeaveEventSubscriber::class,
    ];
}
