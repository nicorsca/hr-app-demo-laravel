<?php

use App\Modules\Holidays\Controllers\HolidaysController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['authApi']], function () {
    Route::post('holidays/multiple', [HolidaysController::class, 'storeMultiple'])->middleware('role:manager');
    Route::apiResource('holidays', 'HolidaysController')->only('index', 'show');
    Route::apiResource('holidays', 'HolidaysController')->only('store', 'update', 'destroy')->middleware('role:manager');
});
