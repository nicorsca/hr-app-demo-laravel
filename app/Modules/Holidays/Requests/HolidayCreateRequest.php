<?php

namespace App\Modules\Holidays\Requests;

use NicoSystem\Requests\NicoRequest;

/**
 * Class HolidayCreateRequest
 * @package App\Modules\Holidays\Requests
 */
class HolidayCreateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:50',
            'date' => 'required|date_format:Y-m-d|after:today|unique:holidays,date,NULL,id,deleted_at,NULL',
            'remarks' => 'nullable|string|max:250',
        ];
    }
}
