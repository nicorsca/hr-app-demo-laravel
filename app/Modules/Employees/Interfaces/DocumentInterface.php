<?php


namespace App\Modules\Employees\Interfaces;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\System\Common\Database\Models\Document as SystemDocument;
use App\Modules\Employees\Database\Models\Document;

interface DocumentInterface
{
    /**
     * @param  array  $params
     * @param  string $employeeId
     * @return Collection|LengthAwarePaginator
     */
    public function getDocumentList(array $params, string $employeeId): Collection|LengthAwarePaginator;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return Document
     */
    public function createDocument(array $inputs, string $employeeId): Document;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @param  string $documentId
     * @return Document
     */
    public function updateDocument(array $inputs, string $employeeId, string $documentId): Document;

    /**
     * @param  string $employeeId
     * @param  string $documentId
     * @return mixed
     */
    public function destroyDocument(string $employeeId, string $documentId): bool;

    /**
     * @param  string $employeeId
     * @param  string $documentId
     * @return Document
     */
    public function getDocumentById(string $employeeId, string $documentId): SystemDocument;

    /**
     * @param string $employeeId
     * @param string $documentId
     * @return \App\System\Common\Database\Models\Document
     */
    public function toggleDocumentStatus(string $employeeId, string $documentId): Document;

}
