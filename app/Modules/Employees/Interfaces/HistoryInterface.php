<?php


namespace App\Modules\Employees\Interfaces;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface HistoryInterface
{

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function getHistoryList(array $inputs, string $employeeId): LengthAwarePaginator|Collection;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return bool
     */
    public function deactivate(array $inputs, string $employeeId): bool;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return bool
     */
    public function activate(array $inputs, string $employeeId): bool;
}
