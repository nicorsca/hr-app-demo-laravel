<?php


namespace App\Modules\Employees\Requests;


use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

class SocialSecurityCreateRequest extends NicoRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:100',
            'number' => 'required|alpha_dash|max:15',
            'start_date' => 'required|date_format:Y-m-d|before_or_equal:now',
            'status' => 'required|in:' . implode(',', Status::options()),
        ];
    }

}
