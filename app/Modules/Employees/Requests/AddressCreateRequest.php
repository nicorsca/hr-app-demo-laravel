<?php


namespace App\Modules\Employees\Requests;


use App\System\Common\Foundation\AddressType;
use NicoSystem\Requests\NicoRequest;

class AddressCreateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'country' => 'required|string|max:60',
            'state' => 'required|string|max:60',
            'city' => 'required|string|max:85',
            'street' => 'required|string|max:85',
            'zip_code' => 'required|integer|digits:5',
            'type' => 'required|distinct|in:' . implode(',', AddressType::options()),
        ];
    }
}
