<?php


namespace App\Modules\Employees\Requests;


use NicoSystem\Requests\NicoRequest;

class HistoryDeactivateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'comment' => 'required|string|max:255',
            'url' => 'nullable|required_with:thumbnail|url|ends_with:jpg,png,jpeg,pdf,doc,docx',
            'thumbnail' => 'nullable|url|ends_with:jpg,png,jpeg,gif,svg',
        ];
    }
}
