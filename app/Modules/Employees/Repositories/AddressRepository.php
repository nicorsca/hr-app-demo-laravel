<?php


namespace App\Modules\Employees\Repositories;


use App\Exceptions\ResourceExistsException;
use App\Modules\Employees\Database\Models\Address;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Filters\AddressesFilter;
use App\Modules\Employees\Interfaces\AddressInterface;
use App\System\Common\Repositories\BaseCommonRepository;
use App\System\Common\Database\Models\Address as SystemAddress;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class AddressRepository extends BaseCommonRepository implements AddressInterface
{
    /**
     * @var \App\Modules\Employees\Database\Models\Employee
     */
    private Employee $globalEmployee;

    /**
     * AddressRepository constructor.
     *
     * @param Employee $employee
     * @param Address  $address
     */
    public function __construct(private Employee $employee, Address $address)
    {
        parent::__construct($address);
    }

    /**
     * @param string $employeeId
     * @param array  $inputs
     */
    public function updateAddresses(string $employeeId, array $inputs): void
    {
        parent::updateRelations($this->employee, 'addresses', $employeeId, $inputs);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return \App\Modules\Employees\Filters\AddressesFilter
     */
    public function getFilter(Builder $builder): AddressesFilter
    {
        return new AddressesFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->where(
            [
            'addressable_id' => $this->globalEmployee->id,
            'addressable_type' => $this->employee->getMorphClass(),
            ]
        );
    }

    /**
     * @param  array  $params
     * @param  string $employeeId
     * @return \Illuminate\Support\Collection|\Illuminate\Pagination\LengthAwarePaginator
     */
    public function getAddressList(array $params, string $employeeId): Collection|LengthAwarePaginator
    {
        $this->globalEmployee = $this->employee->findOrfail($employeeId);

        return parent::getList($params);
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return \App\Modules\Employees\Database\Models\Address
     */
    public function createAddress(array $inputs, string $employeeId): Address
    {
        $employee = $this->employee->findOrfail($employeeId);
        if ($employee->addresses()->where('type', $inputs['type'])->first()) {
            throw new ResourceExistsException(trans('responses.employee.address.already_exists'), 'err_address_exists_exception');
        }
        $inputs['addressable_id'] = $employeeId;
        $inputs['addressable_type'] = $this->employee->getMorphClass();

        return parent::create($inputs);
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @param  string $addressId
     * @return \App\Modules\Employees\Database\Models\Address
     */
    public function updateAddress(array $inputs, string $employeeId, string $addressId): Address
    {
        $this->getAddressById($employeeId, $addressId);

        return parent::update($addressId, $inputs);
    }

    /**
     * @param  string $employeeId
     * @param  string $addressId
     * @return bool
     */
    public function destroyAddress(string $employeeId, string $addressId): bool
    {
        $address = $this->getAddressById($employeeId, $addressId);

        return $address->delete();
    }

    /**
     * @param  string $employeeId
     * @param  string $addressId
     * @return \App\System\Common\Database\Models\Address
     */
    public function getAddressById(string $employeeId, string $addressId): SystemAddress
    {
        $employee = $this->employee->findOrfail($employeeId);

        return $employee->addresses()->findOrfail($addressId);
    }
}
