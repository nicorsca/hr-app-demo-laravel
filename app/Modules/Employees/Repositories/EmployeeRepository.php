<?php

namespace App\Modules\Employees\Repositories;

use App\Events\Employee\Created;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\Setting;
use App\Modules\Employees\Database\Models\User;
use App\Modules\Employees\Interfaces\EmployeeInterface;
use App\System\User\Foundation\RoleName;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use NicoSystem\Exceptions\NicoException;
use NicoSystem\Repositories\BaseRepository;
use Swift_SwiftException;

/**
 * Class EmployeeRepository
 *
 * @package App\Modules\Employees\Repositories
 */
class EmployeeRepository extends BaseRepository implements EmployeeInterface
{
    /**
     * EmployeeRepository constructor.
     *
     * @param Employee $employee
     * @param User $user
     * @param Setting $setting
     */
    public function __construct(
        Employee $employee,
        private User $user,
        private Setting $setting,
    ) {
        parent::__construct($employee);
    }

    /**
     * @param Builder $builder
     *
     * @return void
     */
    public function getFilter(Builder $builder): void
    {
    }

    /**
     * @param array $inputs
     *
     * @return Employee
     */
    public function create(array $inputs): Employee
    {
        $inputs['password'] = Hash::make(Str::random(10));
        DB::beginTransaction();
        try {
            $user              = $this->user->create(
                Arr::only(
                    $inputs,
                    ['first_name', 'last_name', 'middle_name', 'email', 'password']
                )
            );
            $inputs['user_id'] = $user->id;
            $inputs['code']    = $this->generateEmployeeCode($inputs['joined_at']);
            $employee          = parent::create(
                Arr::only(
                    $inputs,
                    [
                        'code',
                        'personal_email',
                        'position',
                        'joined_at',
                        'gender',
                        'dob',
                        'marital_status',
                        'pan_no',
                        'department_id',
                        'user_id'
                    ]
                )
            );
            $user->assignRole(RoleName::EMPLOYEE);
            event(new Created($employee, $this->setting));
            DB::commit();
            return $employee;
        } catch (QueryException | Swift_SwiftException | Exception $e) {
            DB::rollBack();
            throw new NicoException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $joinedAt
     *
     * @return string
     */
    private function generateEmployeeCode(string $joinedAt): string
    {
        $date     = Carbon::parse($joinedAt);
        $prefix   = 'EN' . $date->year;
        $employee = $this->model->where('code', 'like', "{$prefix}%")
            ->withTrashed()
            ->orderBy('joined_at', 'asc')
            ->orderBy('code', 'asc')
            ->get()->last();
        if (!$employee) {
            return $prefix . '01';
        }
        $nextHireNumber = substr($employee->code, -2) + 1;
        $suffix         = str_pad($nextHireNumber, 2, '0', STR_PAD_LEFT);
        $code           = $prefix . $suffix;

        for ($i = 1; $i <= 5; $i++) {
            if ($this->checkCodeExist($code)) {
                break;
            } else {
                $code = $prefix . ((int)++$suffix);
            }
        }
        return $code;
    }

    /**
     * @param $code
     *
     * @return bool
     */
    private function checkCodeExist($code): bool
    {
        $codeCount = $this->model->where('code', $code)->count();
        return $codeCount == 0;
    }

    /**
     * @param string $id
     * @param array $attributes
     *
     * @return Employee
     */
    public function update(string $id, array $attributes): Employee
    {
        DB::beginTransaction();
        try {
            $employee = parent::update($id, Arr::only($attributes,
                ['personal_email', 'position', 'gender', 'dob', 'marital_status', 'pan_no', 'department_id']));
            $employee->user()->update(Arr::only($attributes, ['first_name', 'last_name', 'middle_name']));
            DB::commit();
            return $employee->fresh();
        } catch (ModelNotFoundException $exception) {
            DB::rollBack();
            throw new ModelNotFoundException();
        } catch (QueryException | Exception $e) {
            DB::rollBack();
            throw new NicoException($e->getMessage());
        }
    }

}
