<?php


namespace App\Modules\Employees\Filters;


use NicoSystem\Filters\BaseFilter;

class AddressesFilter extends BaseFilter
{
    /**
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
    }

    /**
     * @param string $title
     */
    public function title(string $title = ''): void
    {
    }
}
