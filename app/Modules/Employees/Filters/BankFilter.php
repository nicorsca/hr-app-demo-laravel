<?php

namespace App\Modules\Employees\Filters;

use NicoSystem\Filters\BaseFilter;

class BankFilter extends BaseFilter
{
    /**
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        $this->name($keyword);
    }

    /**
     * @param string $title
     */
    public function title(string $title = ''): void
    {
        $this->name($title);
    }

    /**
     * @param string $name
     */
    public function name(string $name = ''): void
    {
        if ($name !== '') {
            $this->builder->where('name', 'like', "%{$name}%");
        }
    }
}
