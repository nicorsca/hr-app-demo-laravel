<?php
namespace App\Modules\Employees\Database\Models;

use App\System\Employee\Database\Models\Employee as BaseModel;

/**
 * Class Employee
 *
 * @package App\Modules\Employees\Database\Models
 */
class Employee extends BaseModel
{
    /**
     * Employee constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->makeHidden('department_id');
    }

    /**
     * @var string[]
     */
    protected $with = [
        'user',
        'department',
    ];
}
