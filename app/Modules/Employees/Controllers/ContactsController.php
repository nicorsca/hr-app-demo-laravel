<?php


namespace App\Modules\Employees\Controllers;


use App\Modules\Employees\Interfaces\ContactInterface;
use App\Modules\Employees\Requests\ContactCreateRequest;
use App\Modules\Employees\Requests\ContactUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

class ContactsController extends BaseController
{
    /**
     * ContactsController constructor.
     *
     * @param \App\Modules\Employees\Interfaces\ContactInterface $repository
     */
    public function __construct(private ContactInterface $repository)
    {
    }

    /**
     * @param  \NicoSystem\Foundation\Requests\NicoListRequest $request
     * @param  string                                          $employeeId
     * @return JsonResponse
     */
    public function index(NicoListRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->getContactList($request->all(), $employeeId));
    }

    /**
     * @param  \App\Modules\Employees\Requests\ContactCreateRequest $request
     * @param  string                                               $employeeId
     * @return JsonResponse
     */
    public function store(ContactCreateRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->createContact($request->only(['number', 'type', 'status']), $employeeId));
    }

    /**
     * @param  \App\Modules\Employees\Requests\ContactUpdateRequest $request
     * @param  string                                               $employeeId
     * @param  string                                               $contactId
     * @return JsonResponse
     */
    public function update(ContactUpdateRequest $request, string $employeeId, string $contactId): JsonResponse
    {
        return $this->responseOk($this->repository->updateContact($request->only(['number', 'type', 'status']), $employeeId, $contactId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  string      $contactId
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $employeeId, string $contactId): JsonResponse
    {
        return $this->responseOk($this->repository->destroyContact($employeeId, $contactId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  $contactId
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $employeeId, $contactId): JsonResponse
    {
        return $this->responseOk($this->repository->getContactById($employeeId, $contactId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  string      $contactId
     * @return JsonResponse
     */
    public function toggleStatus(NicoRequest $request, string $employeeId, string $contactId): JsonResponse
    {
        return $this->responseOk($this->repository->toggleContactStatus($employeeId, $contactId));
    }
}
