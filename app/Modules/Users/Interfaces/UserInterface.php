<?php

namespace App\Modules\Users\Interfaces;

use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface UserInterface
 *
 * @package App\Modules\Users\Interfaces
 */
interface UserInterface extends BasicCrudInterface
{

}
