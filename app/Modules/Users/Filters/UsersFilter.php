<?php

namespace App\Modules\Users\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class UsersController
 *
 * @package App\Modules\Users\Filters
 */
class UsersFilter extends BaseFilter
{
    /**
     * To do filter here
     *
     * @param $keyword
     */
    public function keyword($keyword)
    {
        parent::title($keyword);
    }

}
