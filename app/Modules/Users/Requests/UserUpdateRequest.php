<?php
namespace App\Modules\Users\Requests;

use NicoSystem\Requests\NicoRequest;

/**
 * Class UserUpdateRequest
 *
 * @package App\Modules\Users\Requests
 */
class UserUpdateRequest extends NicoRequest
{

}
