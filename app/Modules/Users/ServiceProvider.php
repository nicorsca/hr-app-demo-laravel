<?php
namespace App\Modules\Users;

use \Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 *
 * @package App\Modules\Users
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected $defer = true;

    /**
     * Register your binding
     */
    public function register()
    {
        $this->app->bind('App\Modules\Users\Interfaces\UserInterface', 'App\Modules\Users\Repositories\UserRepository');
    }
}
