<?php

namespace App\Modules\UserPreferences\Repositories;

use App\Modules\UserPreferences\Filters\UserPreferencesFilter;
use App\Modules\UserPreferences\Interfaces\UserPreferenceInterface;
use App\Modules\UserPreferences\Database\Models\UserPreference;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use NicoSystem\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserPreferenceRepository
 * @package App\Modules\UserPreferences\Repositories
 */
class UserPreferenceRepository extends BaseRepository implements UserPreferenceInterface
{
    /**
     * @var int
     */
    private int $userId;

    /**
     * UserPreferenceRepository constructor.
     * @param UserPreference $model
     */
    public function __construct(UserPreference $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     * @return UserPreferencesFilter
     */
    public function getFilter(Builder $builder): UserPreferencesFilter
    {
        return new UserPreferencesFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->where('user_id', $this->userId);
    }

    /**
     * @param int $userId
     * @param array $params
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getUserPreferencesList(int $userId, array $params = []): LengthAwarePaginator
    {
        $this->userId = $userId;
        return parent::getList($params);
    }

    /**
     * @param string $preferenceId
     * @param int $userId
     * @param array $inputs
     * @return \App\Modules\UserPreferences\Database\Models\UserPreference
     */
    public function updateUserPreference(string $preferenceId, int $userId, array $inputs): UserPreference
    {
        if (is_array($inputs['value']) && Arr::has($inputs['value'], ['start_work_time', 'end_work_time'])) {
            $inputs['value'] = $inputs['value']['start_work_time'] . '-' . $inputs['value']['end_work_time'];
        }
        $this->getUserPreferenceById($preferenceId, $userId);

        return parent::update($preferenceId, $inputs);
    }

    /**
     * @param string $preferenceId
     * @param int $userId
     * @return mixed
     */
    public function getUserPreferenceById(string $preferenceId, int $userId): UserPreference
    {
        return $this->model->where('user_id', $userId)->findOrfail($preferenceId);
    }

}
