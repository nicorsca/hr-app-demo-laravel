<?php

namespace App\Modules\UserPreferences\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class UserPreferencesController
 * @package App\Modules\UserPreferences\Filters
 */
class UserPreferencesFilter extends BaseFilter
{
    /**
     * To do filter here
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
       $this->key($keyword);
    }

    /**
     * @param string $key
     */
    public function key(string $key = ''): void
    {
        if ($key !== '') {
            $this->builder->where('key', 'like', "%{$key}%");
        }
    }

    /**
     * @param string $category
     */
    public function category(string $category = ''): void
    {
        if ($category !== '') {
            $this->builder->where('category', $category);
        }
    }

    /**
     * @param string $status
     */
    public function status(string $status = ''): void
    {
    }
}
