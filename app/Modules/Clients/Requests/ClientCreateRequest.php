<?php
namespace App\Modules\Clients\Requests;

use NicoSystem\Requests\NicoRequest;

/**
 * Class ClientCreateRequest
 * @package App\Modules\Clients\Requests
 */
class ClientCreateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:100',
            'avatar' => 'nullable|url|ends_with:.jpg,.png,.jpeg|max:255',
        ];
    }
}
