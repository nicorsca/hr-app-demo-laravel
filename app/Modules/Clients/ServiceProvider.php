<?php
namespace App\Modules\Clients;

use App\Modules\Clients\Providers\EventServiceProvider;
use \Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 * @package App\Modules\Clients
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected $defer = true;

    /**
     * Register your binding
     */
    public function register()
    {
        $this->app->bind('App\Modules\Clients\Interfaces\ClientInterface','App\Modules\Clients\Repositories\ClientRepository');
        $this->app->register(EventServiceProvider::class);
    }
}
