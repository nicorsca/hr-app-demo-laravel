<?php

namespace App\Modules\ActivityLogs\Repositories;

use App\Modules\ActivityLogs\Database\Models\ActivityLog;
use App\Modules\ActivityLogs\Filters\ActivityLogsFilter;
use App\Modules\ActivityLogs\Interfaces\ActivityLogInterface;
use Illuminate\Database\Eloquent\Builder;
use NicoSystem\Repositories\BaseRepository;

/**
 * Class ActivityLogRepository
 * @package App\Modules\ActivityLogs\Repositories
 */
class ActivityLogRepository extends BaseRepository implements ActivityLogInterface
{
    /**
     * ActivityLogRepository constructor.
     * @param ActivityLog $model
     */
    public function __construct(ActivityLog $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     * @return ActivityLogsFilter
     */
    public function getFilter(Builder $builder): ActivityLogsFilter
    {
        return new ActivityLogsFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->with(['model', 'user']);
    }

    /**
     * @param string $id
     * @param array $attributes
     * @return \App\Modules\ActivityLogs\Database\Models\ActivityLog
     */
    public function getById(string $id, array $attributes = []): ActivityLog
    {
        return $this->model->with('model', 'user')->findOrFail($id);
    }
}
