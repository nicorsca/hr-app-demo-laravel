<?php

namespace App\Modules\ActivityLogs\Providers;

use App\Modules\ActivityLogs\Listeners\ActivityLogEventSubscriber;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        ActivityLogEventSubscriber::class
    ];
}
