<?php
/**
 * @OA\Schema(
 *  schema="UserPreferenceCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",    type="integer", default="1", readOnly=true),
 * @OA\Property(property="key",   type="string"),
 * @OA\Property(property="value", type="string"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="UserPreferenceModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/UserPreferenceCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="category",       type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */


/**
 * @OA\Schema(
 *  schema="UserPreferenceResponseModel",
 *  allOf={
 * @OA\Schema  (ref="#/components/schemas/UserPreferenceModel"),
 *  }
 * )
 */


/**
 * @OA\Schema(
 *  schema="UserPreferenceListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/UserPreferenceModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */
