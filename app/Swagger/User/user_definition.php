<?php
/**
 * @OA\Schema(
 *  schema="UserCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",          type="integer", default="1", readOnly=true),
 * @OA\Property(property="first_name",  type="string"),
 * @OA\Property(property="last_name",   type="string"),
 * @OA\Property(property="middle_name", type="string"),
 * @OA\Property(property="email",       type="string"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="UserModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/UserCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="avatar",     type="string"),
 * @OA\Property(property="status",     type="boolean"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 * @OA\Property(property="name",       type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema                           (
 *  schema="ProvidersModel",
 * @OA\Property(property="id",          type="integer", default="1", readOnly=true),
 * @OA\Property(property="provider_id", type="string"),
 * @OA\Property(property="provider",    type="string"),
 * @OA\Property(property="avatar",      type="string"),
 * @OA\Property(property="created_at",  type="string"),
 * @OA\Property(property="updated_at",  type="string"),
 * )
 */

/**
 * @OA\Schema                                       (
 *     schema="UserResponseModel",
 *      allOf={
 * @OA\Schema(ref="#/components/schemas/UserModel")
 *     }
 * )
 */

/**
 * @OA\Schema(
 *  schema="UserListResponseModel",
 *  allOf={
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(ref="#/components/schemas/UserModel")),
 *     )
 * }
 * )
 */

/**
 * @OA\Schema(
 *  schema="UserDetailResponseModel",
 *  allOf={
 * @OA\Schema(ref="#/components/schemas/UserModel")
 *   }
 * )
 */
