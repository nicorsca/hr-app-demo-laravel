<?php

/**
 * @OA\OpenApi(
 * @OA\Info(
 *         version="1.0",
 *         title="ENSUE HR App",
 *         description="Swagger documentation for ENSUE HR App"
 *     ),
 * )
 */

/**
 * @OA\SecurityScheme(
 *     type="http",
 *     in="header",
 *     securityScheme="api_key",
 *     scheme="bearer",
 *     name="Authorization"
 * )
 */
