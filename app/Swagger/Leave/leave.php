<?php
/**
 * @OA\Get(
 *     path="/api/leaves",
 *     summary="Fetch list of leaves",
 *     tags={"Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"title", "start_at", "end_at", "created_at", "status"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by title",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="start_at",
 *         in="query",
 *         description="leaves greater or equal to start at",
 * @OA\Schema(
 *             type="string",
 *             default="2021-01-01 00:00:00"
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="end_at",
 *         in="query",
 *         description="leaves less than or equal to end at: required with start_at",
 * @OA\Schema(
 *             type="string",
 *             default="2021-01-01 00:00:00"
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="employee_id",
 *         in="query",
 *         description="filter by employee id",
 * @OA\Schema(
 *             type="integer",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="requested_to",
 *         in="query",
 *         description="filter by requested to id",
 * @OA\Schema(
 *             type="integer",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="type",
 *         in="query",
 *         description="search by type listed in system settings including UNPAID and CANCELLATION by default",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="status",
 *         in="query",
 *         description="Filter by status: 0=CANCELLED, 1=PENDING, 2=APPROVED",
 * @OA\Schema(
 *             type="string",
 *             enum={"1","0","2"}
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LeaveListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Post                                                       (
 *     path="/api/leaves",
 *     summary="Create new leave request",
 *     tags={"Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/LeaveRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LeaveModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/leaves/{leave_id}",
 *     summary="Get detail of a leave request",
 *     tags={"Leaves"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="leave_id",
 *         in="path",
 *         description="ID of a leave request",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LeaveResponseModel")
 *     )
 * )
 */
/**
 * @OA\Put(
 *     path="/api/leaves/{leave_id}",
 *     summary="Update a leave request",
 *     tags={"Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="leave_id",
 *         in="path",
 *         description="ID of a leave",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/LeaveRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LeaveResponseModel"),
 *     )
 * )
 */

/**
 * @OA\Post                                                                (
 *     path="/api/leaves/emergency",
 *     summary="Create new leave request for employees by manager",
 *     tags={"Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/LeaveEmergencyRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LeaveResponseModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Post                                                       (
 *     path="/api/leaves/{leave_id}/cancellation",
 *     summary="Create new cancellation leave request",
 *     tags={"Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="leave_id",
 *         in="path",
 *         description="ID of a (PAID/UNPAID) leave whose cancellation leave request is to be done",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(
 *      @OA\Property(property="title",        type="string", default="Application for leave request"),
 *      @OA\Property(property="description",  type="string"),
 *      @OA\Property(property="start_at",     type="string", default="YYYY-MM-DD H:i:s"),
 *      @OA\Property(property="end_at",       type="string", default="YYYY-MM-DD H:i:s"),
 *     ),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LeaveModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Put(
 *     path="/api/leaves/{leave_id}/approve",
 *     summary="Approve a leave request by requested to employee only",
 *     tags={"Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="leave_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LeaveModel"),
 *     )
 * )
 */

/**
 * @OA\Put(
 *     path="/api/leaves/{leave_id}/cancel",
 *     summary="Cancel a leave request by requested employee",
 *     tags={"Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="leave_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(@OA\Property (property="reason", type="string")),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LeaveModel"),
 *     )
 * )
 */

/**
 * @OA\Put(
 *     path="/api/leaves/{leave_id}/decline",
 *     summary="Decline a leave request by requested requested to employee",
 *     tags={"Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="leave_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(@OA\Property (property="reason", type="string")),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LeaveModel"),
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/api/leaves/summary",
 *     summary="Get summary of the employee leaves of current fiscal year",
 *     tags={"Leaves"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="query",
 *         description="filter by employee id",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LeaveSummaryResponseModel")
 *     )
 * )
 */
