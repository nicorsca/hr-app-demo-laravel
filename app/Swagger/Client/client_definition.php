<?php
/**
 * @OA\Schema(
 *  schema="ClientCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",          type="integer", default="1", readOnly=true),
 * @OA\Property(property="name",       type="string"),
 * @OA\Property(property="avatar",       type="string"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="ClientModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/ClientCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="ClientListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/ClientModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */


