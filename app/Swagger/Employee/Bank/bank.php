<?php

/**
 * @OA\Get(
 *     path="/api/employees/{employee_id}/banks",
 *     summary="Fetch list of employee banks",
 *     tags={"Employee Banks"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"name","branch","account_number","status","created_at"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by name",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="status",
 *         in="query",
 *         description="Filter by status: 1=UNPUBLISHED, 2=PUBLISHED",
 * @OA\Schema(
 *             type="string",
 *             enum={"1","2"}
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/BankListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Post                                                     (
 *     path="/api/employees/{employee_id}/banks",
 *     summary="Add new employee bank account to the organization",
 *     tags={"Employee Banks"},
 *     security={
 *           {"api_key": {}}
 *       },
 * *     @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/BankCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/BankModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/employees/{employee_id}/banks/{bank_id}",
 *     summary="Get detail of a employee bank account",
 *     tags={"Employee Banks"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="bank_id",
 *         in="path",
 *         description="ID of a bank",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/BankModel")
 *     )
 * )
 */


/**
 * @OA\Put(
 *     path="/api/employees/{employee_id}/banks/{bank_id}",
 *     summary="Update a employee bank account",
 *     tags={"Employee Banks"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 * @OA\Parameter(
 *         name="bank_id",
 *         in="path",
 *         description="ID of a bank",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/BankCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/BankModel"),
 *     )
 * )
 */

/**
 * @OA\Delete                      (
 *     path="/api/employees/{employee_id}/banks/{bank_id}",
 *     summary="Delete a emplooyee bank account",
 *     tags={"Employee Banks"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 * @OA\Parameter(
 *         name="bank_id",
 *         in="path",
 *         description="ID of a bank",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object"),
 *     )
 * )
 */

/**
 * @OA\Put(
 *     path="/api/employees/{employee_id}/banks/{bank_id}/status",
 *     summary="Toggle the status of bank",
 *     tags={"Employee Banks"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="bank_id",
 *         in="path",
 *         description="ID of a Bank",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/BankModel"),
 *     )
 * )
 */
