<?php

/**
 * @OA\Get(
 *     path="/api/employees/{employee_id}/addresses",
 *     summary="Fetch list of employee address",
 *     tags={"Employee Addresses"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"type"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AddressListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/employees/{employee_id}/addresses/{address_id}",
 *     summary="Get detail of a address",
 *     tags={"Employee Addresses"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Parameter(
 *         name="address_id",
 *         in="path",
 *         description="ID of a address",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AddressResponseModel")
 *     )
 * )
 */

/**
 * @OA\Post                                                               (
 *     path="/api/employees/{employee_id}/addresses",
 *     summary="Create new address",
 *     tags={"Employee Addresses"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/AddressCreateRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AddressResponseModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Put(
 *     path="/api/employees/{employee_id}/addresses/{address_id}",
 *     summary="Update a address",
 *     tags={"Employee Addresses"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="address_id",
 *         in="path",
 *         description="ID of a address",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/AddressCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AddressResponseModel"),
 *     )
 * )
 */

/**
 * @OA\Delete(
 *     path="/api/employees/{employee_id}/addresses/{address_id}",
 *     summary="Delete a address",
 *     tags={"Employee Addresses"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="address_id",
 *         in="path",
 *         description="ID of a Address",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object")
 *     ),
 * )
 */
