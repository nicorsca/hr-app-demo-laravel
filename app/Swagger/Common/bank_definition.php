<?php
/**
 * @OA\Schema(
 *  schema="BankCommonModel",
 * @OA\Property(property="id",             type="integer", default="1", readOnly=true),
 * @OA\Property(property="name",           type="string"),
 * @OA\Property(property="branch",         type="string"),
 * @OA\Property(property="account_number", type="string"),
 * @OA\Property(property="status",         type="integer"),
 * )
 */

/**
 * @OA\Schema(
 *  schema="BankModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/BankCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="bankable_id",       type="integer"),
 * @OA\Property(property="bankable_type",       type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="BankListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/BankModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */
