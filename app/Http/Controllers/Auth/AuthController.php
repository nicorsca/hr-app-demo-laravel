<?php

namespace App\Http\Controllers\Auth;


use App\Events\User\Updated;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAvatarRequest;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use NicoSystem\Requests\NicoRequest;

class AuthController extends Controller
{
    /**
     * @param NicoRequest $request
     * @return JsonResponse
     */
    public function getAuthUser(NicoRequest $request): JsonResponse
    {
        $user = Auth::user();
        $user->employeeView;
        return $this->responseOk($user);
    }

    /**
     * @param \NicoSystem\Requests\NicoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthEmployeeDetail(NicoRequest $request): JsonResponse
    {
        $user = Auth::user();
        $employee = $user->employeeView()->with([
            'addresses' => function (MorphMany $query) {
                $query->published();
            },
            'contacts' => function (MorphMany $query) {
                $query->published();
            },
            'bank' => function (MorphOne $query) {
                $query->published();
            },
            'socialSecurities' => function (HasMany $query) {
                $query->published();
            },
            'allocatedLeaves' => function (HasMany $query) {
                $query->currentFiscalYear()->published();
            },
            'documents' => function (MorphMany $query) {
                $query->published();
            }
        ])->first();

        return $this->responseOk($employee);
    }

    /**
     * @param \App\Http\Requests\UpdateAvatarRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAvatar(UpdateAvatarRequest $request): JsonResponse
    {
        $user = Auth::user();
        $user->avatar = $request->get('avatar');
        $user->save();
        event(new Updated($user));

        return $this->responseOk($user);
    }
}
