<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\System\User\Database\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ChangePasswordController extends Controller
{
    /**
     * @param ChangePasswordRequest $request
     * @return JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request): JsonResponse
    {
        $user = Auth::user();
        $user->email_verified_at = $user->email_verified_at ?? now();
        $user->password = bcrypt($request->new_password);
        $user->is_locked = User::STATUS_UNLOCKED;
        $user->save();
        return $this->responseOk($user, trans("auth.password_success"));
    }
}
