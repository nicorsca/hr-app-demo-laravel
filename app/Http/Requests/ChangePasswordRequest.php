<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\Validator;
use NicoSystem\Requests\NicoRequest;

class ChangePasswordRequest extends NicoRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'old_password' => 'required|string',
            'new_password' => ['required','string', 'confirmed',
                Password::min(8)->letters()->mixedCase()->numbers()->symbols()->uncompromised(3)
            ],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function ($validator) {
            if ($this->has('old_password') && !Hash::check($this->old_password, Auth::user()->password)) {
                $validator->errors()->add('old_password', trans('auth.old_password'));
            }

            if ($this->has('new_password') && $this->old_password === $this->new_password) {
                $validator->errors()->add('new_password', trans('auth.recent_password'));
            }
        });
    }
}
