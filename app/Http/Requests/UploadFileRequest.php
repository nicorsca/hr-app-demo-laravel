<?php

namespace App\Http\Requests;

use NicoSystem\Requests\NicoRequest;

class UploadFileRequest extends NicoRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => 'required|file|mimes:jpg,png,jpeg,gif,svg,pdf,doc,docx,xls,xlsx'
        ];
    }
}
