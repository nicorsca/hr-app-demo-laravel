<?php

namespace App\Http\Requests;

use NicoSystem\Requests\NicoRequest;

class UpdateAvatarRequest extends NicoRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'avatar' => 'nullable|url|ends_with:.jpg,.png,.jpeg|max:255|starts_with:' . config('fileupload.whitelist_urls'),
        ];
    }
}
