<?php


namespace App\Exceptions;


use NicoSystem\Exceptions\NicoException;

class ForbiddenException extends NicoException
{

    protected $code = 403;

    protected string $respCode = 'forbidden';

}
