<?php

namespace App\Exceptions;

use App\System\AppConstants;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use NicoSystem\Exceptions\NicoException;
use NicoSystem\Foundation\NicoResponseTraits;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use NicoResponseTraits;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Throwable $exception
     * @return void
     * @throws Throwable
     */
    public function report(Throwable $exception)
    {
        if ($exception->getCode() == 500 && config('app.send_error_mail_to_developer', false)) {
            $this->sendErrorReportViaMail($exception);
        }
        parent::report($exception);
    }

    /**
     * Send Exception error Report
     * @param \Throwable $exception
     */
    protected function sendErrorReportViaMail(Throwable $exception)
    {
        $content = $this->prepareResponse(request(), $exception);
        try {
            Mail::send('errors.developer-mail', compact('content'), function ($message) {
                $message->to(config('mail.developer.email'), config('mail.developer.name'));
                $subject = config('app.name') . ": Error From " . env("APP_ENV");
                $message->subject($subject);
            });
        } catch (\Exception $exception) {
            //Do nothing if error mail is not sent because it is not compulsory
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param Throwable $e
     * @return \Symfony\Component\HttpFoundation\Response|JsonResponse
     * @throws \Throwable
     */
    public function render($request, Throwable $e): \Symfony\Component\HttpFoundation\Response|JsonResponse
    {
        if ($this->isApiRequest($request)) {
            if ($request->getMethod() == 'OPTIONS' && $e instanceof HttpException) {
                return new Response();
            }

            if ($e instanceof NicoException) {
                return $this->responseError($e->getCode(), $e->getMessage(), $e->getResponseCode());
            } elseif ($e instanceof AuthenticationException) {
                return $this->responseUnAuthorize($e->getMessage());
            } elseif ($e instanceof \Swift_TransportException) {
                return $this->responseServerError("E-mail couldn't be sent due to authorization error. Please check smtp configuration", AppConstants::ERR_MAIL_SERVER_AUTH_OR_CONF);
            } elseif ($e instanceof ValidationException) {
                return $this->responseValidationError($e->errors());
            } elseif ($e instanceof ModelNotFoundException) {
                return $this->responseNotFound();
            } elseif ($e instanceof HttpException) {
                return $this->responseError($e->getStatusCode(), $e->getMessage(), AppConstants::getAppMsgCodeFromStatusCode($e->getStatusCode()));
            }

            return $this->responseError((int)$e->getCode(), $e->getMessage() . " " . $e->getFile() . ": line " . $e->getLine(), $e->getCode());
        }

        return parent::render($request, $e);
    }

    /**
     * Test if the request is an API call
     * @param Request $request
     * @return boolean
     */
    protected function isApiRequest(Request $request): bool
    {
        return is_api_request($request);
    }
}
