<?php


namespace App\Events\Employee\Document;


use App\System\Common\Database\Models\Document;

class Updated
{

    /**
     * Created constructor.
     * @param \App\System\Common\Database\Models\Document $document
     */
    public function __construct(public Document $document)
    {
    }
}
