<?php

namespace App\Events\User;

use App\System\User\Database\Models\User;

class Updated
{

    /**
     * @param \App\System\User\Database\Models\User $user
     */
    public function __construct(public User $user)
    {
    }

}
