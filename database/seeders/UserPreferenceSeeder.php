<?php

namespace Database\Seeders;

use App\System\Setting\Foundation\SettingCategory;
use App\System\User\Database\Models\User;
use App\System\UserPreference\Database\Models\UserPreference;
use App\System\UserPreference\Foundation\UserPreferenceKey;
use App\System\UserPreference\Foundation\UserPreferenceValue;
use Illuminate\Database\Seeder;

class UserPreferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $preferences = [
            UserPreferenceKey::EMPLOYEE_WORK_TIME => [
                'value' => UserPreferenceValue::EMPLOYEE_WORK_TIME_VALUE,
                'category' => SettingCategory::SYSTEM,
            ],
            UserPreferenceKey::USER_THEME => [
                'value' => UserPreferenceValue::DARK_THEME,
                'category' => SettingCategory::UI,
            ],
            UserPreferenceKey::USER_TIMEZONE => [
                'value' => UserPreferenceValue::USER_TIMEZONE_VALUE,
                'category' => SettingCategory::SYSTEM,
            ],
            UserPreferenceKey::USER_LIST_VIEW => [
                'value' => UserPreferenceValue::GRID_VIEW,
                'category' => SettingCategory::UI,
            ]
        ];

        $users = User::all();
        $users->each(function ($user) use ($preferences) {
            foreach ($preferences as $key => $preference) {
                $preference['user_id'] = $user->id;
                UserPreference::setActivityRecord(false)->firstOrCreate([
                    'key' => $key, 'user_id' => $preference['user_id']],
                    $preference
                );
            }
        });
    }
}
