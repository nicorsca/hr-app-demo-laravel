<?php

namespace Database\Factories;

use App\System\Common\Database\Models\Bank;
use Illuminate\Database\Eloquent\Factories\Factory;
use NicoSystem\Foundation\Status;

class BankFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bank::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'branch' => $this->faker->address,
            'account_number' => $this->faker->bankAccountNumber,
            'status' => Status::STATUS_PUBLISHED,
        ];
    }
}
