<?php

namespace Database\Factories;

use App\System\Employee\Database\Models\Employee;
use App\System\Employee\Foundation\Gender;
use App\System\Employee\Foundation\MaritalStatus;
use Illuminate\Database\Eloquent\Factories\Factory;
use NicoSystem\Foundation\Status;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'code' => $this->faker->unique()->randomElement(['EN'. random_int(1000000, 9999999)]),
            'personal_email' => $this->faker->unique()->email,
            'position' => $this->faker->jobTitle,
            'joined_at' => $this->faker->date('Y-m-d'),
            'detached_at' => null,
            'gender' => $this->faker->randomElement(Gender::options()),
            'dob' => $this->faker->date('Y-m-d'),
            'marital_status' =>$this->faker->randomElement(MaritalStatus::options()),
            'pan_no' => $this->faker->randomNumber(9),
        ];
    }
}
