<?php

namespace Database\Factories;

use App\System\Common\Database\Models\Address;
use App\System\Common\Foundation\AddressType;
use Illuminate\Database\Eloquent\Factories\Factory;
use NicoSystem\Foundation\Status;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'country' => $this->faker->country,
            'state' => $this->faker->state,
            'city' => $this->faker->city,
            'street' => $this->faker->streetName,
            'zip_code' => rand(10000, 99999),
            'type' => $this->faker->unique(true)->randomElement(AddressType::options()),
            'status' => Status::STATUS_PUBLISHED,
        ];
    }
}
