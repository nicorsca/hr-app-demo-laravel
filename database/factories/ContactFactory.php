<?php

namespace Database\Factories;

use App\System\Common\Database\Models\Contact;
use App\System\Common\Foundation\ContactType;
use Illuminate\Database\Eloquent\Factories\Factory;
use NicoSystem\Foundation\Status;

class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'number' => $this->faker->phoneNumber,
            'type' => $this->faker->randomElement(ContactType::options()),
            'status' => Status::STATUS_PUBLISHED,
        ];
    }
}
