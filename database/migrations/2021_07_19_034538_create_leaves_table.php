<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavesTable extends Migration
{
    use \Database\Traits\EditorTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 100);
            $table->text('description');
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->unsignedInteger('days');
            $table->string('type', 50);
            $table->boolean('status');
            $table->boolean('is_emergency');
            $table->unsignedBigInteger('requested_to');
            $table->unsignedBigInteger('employee_id');
            $table->text('reason')->nullable();
            $this->editorLogs($table);

            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('requested_to')->references('id')->on('employees')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
