<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    use \Database\Traits\EditorTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('country', 60);
            $table->string('state', 60);
            $table->string('city', 85);
            $table->string('street', 85);
            $table->unsignedBigInteger('zip_code');
            $table->unsignedInteger('type');
            $table->boolean('status');
            $table->unsignedBigInteger('addressable_id');
            $table->string('addressable_type');
            $this->editorLogs($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
