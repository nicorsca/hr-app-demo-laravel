<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 12/29/2016
 * Time: 10:40 PM
 */

return [
    /**
     * Where the modules resides
     */
    'module' => 'Modules',
    'system' => 'System',
    'angular_app' => 'hr-angular'
];
