<?php

return [
    "subject" => config("app.name") . ": HOD access revoked",
    "title" => "Access revoked",
    "body" => "<p>Your permission as Head of Department from :department Department has been revoked.</p>
                <p>Thank you for serving as Head of Department in ENSUE Nepal.</p>",
];
