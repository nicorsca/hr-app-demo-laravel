<?php
return [
    "subject" => config("app.name") . ": Access revoked",
    "title" => "Invitation to the project",
    "body" => "<p>Please login to the Ensue Bot inorder to access the :title project.</p>",
];
